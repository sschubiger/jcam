package ch.bluecc.jcam.gcode;

public class XZ extends GCode {
	private double x;
	private double z;
	
	public XZ(double x, double z) {
		this.x = x;
		this.z = z;
	}

	public double getX() {
		return x;
	}
	
	public double getZ() {
		return z;
	}
}
