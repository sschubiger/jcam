package ch.bluecc.jcam.gcode;

public class G73 extends GCode {
	private int count;
	
	public G73(int count) {
		this.count = count;
	}

	public int getCount() {
		return count;
	}
}
