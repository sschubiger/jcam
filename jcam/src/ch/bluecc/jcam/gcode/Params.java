package ch.bluecc.jcam.gcode;

public class Params {
	private double feedrate_mm_min;
	private int    toolId;
	private double spindleSpeed;
	
	public Params(double feedrate_mm_min, int toolId, double spindleSpeed) {
		this.feedrate_mm_min = feedrate_mm_min;
		this.toolId          = toolId;
		this.spindleSpeed    = spindleSpeed;
	}

	public Params spindleSpeed(double speedRPM) {
		return new Params(feedrate_mm_min, toolId, speedRPM);
	}

	public Params feedRate(double feedrate_mm_min) {
		return new Params(feedrate_mm_min, toolId, spindleSpeed);
	}

	public Params tool(int toolId) {
		return new Params(feedrate_mm_min, toolId, spindleSpeed);
	}
	
	public double getFeedrate_mm_min() {
		return feedrate_mm_min;
	}
	
	public double getSpindleSpeedRPM() {
		return spindleSpeed;
	}

	public int getToolId() {
		return toolId;
	}
	
	@Override
	public boolean equals(Object obj) {
		return 
				obj instanceof Params &&
				((Params)obj).feedrate_mm_min     == feedrate_mm_min &&
				((Params)obj).toolId       == toolId &&
				((Params)obj).spindleSpeed == spindleSpeed;
	}
	
	@Override
	public int hashCode() {
		return (int) (Double.doubleToLongBits(feedrate_mm_min) ^ toolId ^ Double.doubleToLongBits(spindleSpeed));
	}
}
