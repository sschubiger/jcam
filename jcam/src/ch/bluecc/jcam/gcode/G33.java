package ch.bluecc.jcam.gcode;

public class G33 extends XZ implements IParams {
	private Params params;

	static final double[] SPEEDS = {
			-Double.MAX_VALUE, 800,
			.35,               800,
			.5,                480,
			1,                 280,
			1.5,               190,
			2,                 150,
			2.5,               120,
			3,                 100,
			2.5,               80,
			+Double.MAX_VALUE, 80,
	};

	private double inOutDiameter;
	private double cutInc;
	private double pitch;

	public G33(double inOutDiameter, double rootDiameter, double cutInc, double length, double pitch, Params params) {
		super(rootDiameter, length);
		this.inOutDiameter = inOutDiameter;
		this.cutInc        = cutInc;
		this.pitch         = pitch;
		this.params        = params.spindleSpeed(calcSpeed(pitch));
	}

	@Override
	public Params getParams() {
		return params;
	}

	public double getInOutDiam() {
		return inOutDiameter;
	}

	public double getCutInc() {
		return cutInc;
	}

	public double getPitch() {
		return pitch;
	}

	public static double calcSpeed(double pitch) {
		int low = 0;
		for(int i = 0; i < SPEEDS.length; i += 2) {
			if(pitch > SPEEDS[i]) low = i;
			else				  break;
		}
		int high = Math.min(low+2, SPEEDS.length-2);
		double w = (pitch-SPEEDS[low])/(SPEEDS[high]-SPEEDS[low]);
		return SPEEDS[low+1]+(SPEEDS[high+1]-SPEEDS[low+1])*w;	
	}
}
