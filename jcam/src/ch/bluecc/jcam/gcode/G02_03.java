package ch.bluecc.jcam.gcode;

@SuppressWarnings("nls")
public class G02_03 extends XZ implements IParams {
	private double radius;
	private Params params;
	
	public G02_03(double x, double z, double radius, Params params) {
		super(x, z);
		this.radius = radius;
		this.params = params;
	}
	
	public double getRadius() {
		return radius;
	}
	
	@Override
	public Params getParams() {
		return params;
	}
	
	@Override
	public String toString() {
		return super.toString() + " R"+radius;
	}
}
