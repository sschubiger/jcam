package ch.bluecc.jcam.gcode;

@SuppressWarnings("nls")
public class GCode {
	@Override
	public String toString() {
		String result = getClass().getSimpleName();
		if(this instanceof XZ)
			result += " X" + ((XZ)this).getX() + " Z" + ((XZ)this).getZ();
		return result;
	}
}
