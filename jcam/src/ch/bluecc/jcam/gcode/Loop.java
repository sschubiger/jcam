package ch.bluecc.jcam.gcode;

import ch.bluecc.jcam.turn.DataModel;

public class Loop extends Program {
	public static class Count {
		public final int    count;
		public final double step;
		
		public Count(DataModel db, double distance) {
			double res    = db.getDriver().getResolution_mm();
			int    cCount = 0;
			int    cStep  = 0;
			int    distance_steps = Math.max(0, (int)(distance / res));
			if(distance_steps > 0) {
				int cutDept_steps  = Math.max(1, (int)(db.getMaterial().cutDepth_mm() / res));
				for(int steps = cutDept_steps; steps > 0; steps--) {
					cCount = distance_steps / steps;
					if(cCount * steps == distance_steps) {
						cStep = steps;
						break;
					}
				}
			}
			count = cCount;
			step  = cStep * res;
		}
	}
	
	public Loop(Count count, GCode ... code) {
		super(new G73(count.count), code, new G06());
	}
}
