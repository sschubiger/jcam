package ch.bluecc.jcam.gcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Program extends GCode {
	private List<GCode> gcodes;

	public Program(GCode ... code) {
		this.gcodes = new ArrayList<GCode>(Arrays.asList(code));
	}

	public Program(GCode prefix, GCode[] code, GCode postfix) {
		this.gcodes = new ArrayList<>();
		this.gcodes.add(prefix);
		this.gcodes.addAll(Arrays.asList(code));
		this.gcodes.add(postfix);
	}

	public List<GCode> getCode() {
		return gcodes;
	}

	public List<GCode> getFlatCode() {
		List<GCode> result = _getFlatCode();
		// motion optimizer
		int oldSize;
		int newSize;
		do {
			oldSize = result.size();
			result = optimize(result.toArray(new GCode[result.size()]));
			newSize = result.size();
		}
		while(newSize < oldSize);
		return result;
	}
	
	private List<GCode> _getFlatCode() {
		List<GCode> result = new ArrayList<>();
		for(GCode code : this.gcodes) {
			if(code instanceof Program)   result.addAll(((Program) code)._getFlatCode());
			else if(code instanceof Loop) result.addAll(((Loop)code).getCode());
			else        				  result.add(code);
		}
		return result;
	}

	List<GCode> optimize(GCode[] gcodes) {
		List<GCode> result = new ArrayList<>();
		GCode lastGCode = null;
		for(GCode gcode : gcodes) {
			if(lastGCode != null) {
				if(lastGCode instanceof G00 && gcode instanceof G00) {
					G00 prev = (G00)lastGCode;
					G00 cur  = (G00)gcode;
					if(cur.getX() == 0 && cur.getZ() == 0) {
						continue;
					} else if(          prev.getX() == 0 &&         cur.getX() == 0 && 
							Math.signum(prev.getZ()) == Math.signum(cur.getZ()) && 
							prev.getParams().equals(cur.getParams())) {
						result.set(result.size()-1, new G00(0, prev.getZ()+cur.getZ(), cur.getParams()));
					} else if(          prev.getZ() == 0 &&         cur.getZ() == 0 && 
							Math.signum(prev.getX()) == Math.signum(cur.getX()) && 
							prev.getParams().equals(cur.getParams())) {
						result.set(result.size()-1, new G00(prev.getX()+cur.getX(), 0, cur.getParams()));
					} else {
						result.add(gcode);
					}
				} else if(lastGCode instanceof G01 && gcode instanceof G01) {
					G01 prev = (G01)lastGCode;
					G01 cur  = (G01)gcode;
					if(cur.getX() == 0 && cur.getZ() == 0) {
						continue;
					} else if(                 prev.getX() == 0 &&         cur.getX() == 0 && 
							Math.signum(prev.getZ()) == Math.signum(cur.getZ()) && 
							prev.getParams().equals(cur.getParams())) {
						result.set(result.size()-1, new G01(0, prev.getZ()+cur.getZ(), cur.getParams()));
					} else if(          prev.getZ() == 0 &&         cur.getZ() == 0 && 
							Math.signum(prev.getX()) == Math.signum(cur.getX()) && 
							prev.getParams().equals(cur.getParams())) {
						result.set(result.size()-1, new G01(prev.getX()+cur.getX(), 0, cur.getParams()));
					} else {
						result.add(gcode);
					}
				} else {
					result.add(gcode);
				}
			} else {
				result.add(gcode);
			}
			lastGCode = result.get(result.size()-1);
		}
		return result;
	}

	public void add(GCode code) {
		this.gcodes.add(code);
	}
}
