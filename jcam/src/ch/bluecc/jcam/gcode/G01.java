package ch.bluecc.jcam.gcode;

public class G01 extends XZ implements IParams {
	private Params params;
	
	public G01(double x, double z, Params params) {
		super(x, z);
		this.params = params;
	}
	
	@Override
	public Params getParams() {
		return params;
	}
}
