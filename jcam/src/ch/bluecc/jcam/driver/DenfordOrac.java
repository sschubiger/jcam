package ch.bluecc.jcam.driver;

import java.awt.BorderLayout;
import java.awt.Dialog.ModalityType;
import java.awt.Window;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import javax.swing.FocusManager;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;

import ch.bluecc.jcam.gcode.G00;
import ch.bluecc.jcam.gcode.G01;
import ch.bluecc.jcam.gcode.G02;
import ch.bluecc.jcam.gcode.G02_03;
import ch.bluecc.jcam.gcode.G03;
import ch.bluecc.jcam.gcode.G06;
import ch.bluecc.jcam.gcode.G33;
import ch.bluecc.jcam.gcode.G50;
import ch.bluecc.jcam.gcode.G71;
import ch.bluecc.jcam.gcode.G73;
import ch.bluecc.jcam.gcode.G90;
import ch.bluecc.jcam.gcode.G91;
import ch.bluecc.jcam.gcode.GCode;
import ch.bluecc.jcam.gcode.IParams;
import ch.bluecc.jcam.gcode.Loop;
import ch.bluecc.jcam.gcode.Loop.Count;
import ch.bluecc.jcam.gcode.M02;
import ch.bluecc.jcam.gcode.M03;
import ch.bluecc.jcam.gcode.M05;
import ch.bluecc.jcam.gcode.Params;
import ch.bluecc.jcam.gcode.Program;
import ch.bluecc.jcam.gcode.XZ;
import ch.bluecc.jcam.turn.DataModel;
import ch.bluecc.jcam.turn.ICutLinear;
import ch.bluecc.jcam.turn.IOffset;
import ch.bluecc.jcam.turn.OpBore;
import ch.bluecc.jcam.turn.OpChamferLeft;
import ch.bluecc.jcam.turn.OpChamferRight;
import ch.bluecc.jcam.turn.OpCutCircleLeft;
import ch.bluecc.jcam.turn.OpCutCircleRight;
import ch.bluecc.jcam.turn.OpDriver;
import ch.bluecc.jcam.turn.OpFace;
import ch.bluecc.jcam.turn.OpGroove;
import ch.bluecc.jcam.turn.OpMaterial;
import ch.bluecc.jcam.turn.OpPart;
import ch.bluecc.jcam.turn.OpThreadExt;
import ch.bluecc.jcam.turn.OpTrepan;
import ch.bluecc.jcam.turn.Operation;
import ch.bluecc.jcam.turn.Tool;
import ch.bluecc.jcam.turn.OpMaterial.Speed;
import ch.bluecc.jcam.turn.Tool.Cut;
import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortList;

@SuppressWarnings("nls")
public class DenfordOrac extends Driver {
	private static final DecimalFormat IFMT = new DecimalFormat("#");
	private static final DecimalFormat FFMT = new DecimalFormat("#.##");

	private static final String NL  = "\r\n";
	private static final String NL2 = "\r\n\r\n";

	private static final int START_OF_HEADER     = 0x01;
	private static final int START_OF_TEXT       = 0x02;
	private static final int END_OF_TEXT         = 0x1E;
	private static final int END_OF_TRANSMISSION = 0x04;

	private static EnumSet<Rate> RATES = EnumSet.of(Rate.RATE_300, Rate.RATE_1200, Rate.RATE_2400);

	@Override
	public String[] getPorts() {
		return SerialPortList.getPortNames();
	}

	@Override
	public EnumSet<Rate> getRates() {
		return RATES;
	}

	private void writePage(OutputStream out, int pageNo, GCode code) throws IOException {
		byte[] page = String.format("PAGE %02d  " + format(code), pageNo).getBytes(); 
		out.write(START_OF_TEXT);
		out.write(page, 0, page.length - 2);
		out.write(END_OF_TEXT);
	}

	private static final int NO_FEEDRATE = 0x01;

	private String format(GCode code) throws IOException {
		if       (code instanceof G00) {
			return "POINT-TO-POINT.G00,G01" + NL2 + xz((XZ)code) + params(((IParams)code).getParams());
		} else if(code instanceof G01) {
			return "POINT-TO-POINT.G00,G01" + NL2 + xz((XZ)code) + params(((IParams)code).getParams());
		} else if(code instanceof G02) {
			return "CIRCULAR-INT..G02,G03" + NL2 + xz((XZ)code) + circ((G02_03)code) + NL + params(((IParams)code).getParams());
		} else if(code instanceof G03) {
			return "CIRCULAR-INT..G02,G03" + NL2 + xz((XZ)code) + circ((G02_03)code) + NL + params(((IParams)code).getParams());
		} else if(code instanceof G06) {
			return "END-DO-LOOP.G06" + NL;
		} else if(code instanceof G33) {
			G33 g33 = (G33)code;
			return 
					"THREADING..G33"+NL+
					"IN/OUT-SIDE.DIAM "+FFMT.format(g33.getInOutDiam())+NL+
					"ROOT-DIAMTER "    +FFMT.format(g33.getX())+NL+
					"CUT.(INCR)..X "   +FFMT.format(g33.getCutInc())+NL+
					"LENGTH..Z "       +FFMT.format(g33.getZ())+NL2+
					"PITCH "           +FFMT.format(g33.getPitch())+NL+
					"STARTS 1"+NL+params(((IParams)code).getParams(), NO_FEEDRATE);
		} else if(code instanceof G50) {
			return "PROGRAM-DATUM G50" + NL2 + xz((XZ)code);
		} else if(code instanceof G71) {
			return "MM-UNITS G71" + NL;
		} else if(code instanceof G73) {
			return "START-DO-LOOP.G73" + NL2 + "COUNT " + ((G73)code).getCount() + NL;
		} else if(code instanceof G90) {
			return "ABSOLUTE-FORMAT..G90" + NL;
		} else if(code instanceof G91) {
			return "INCREMENTAL-FORMAT.G91" + NL;
		} else if(code instanceof M02) {
			return "END-PROGRAM..M02" + NL;
		} else if(code instanceof M03) {
			return "START-SPINDLE.M03" + NL;
		} else if(code instanceof M05) {
			return "STOP-SPINDLE.M05" + NL;
		}
		throw new IOException("GCode " + code.getClass().getName() + " not supported by Orac");
	}

	private String circ(G02_03 code) {
		return "RADIUS " + FFMT.format(code.getRadius()) + NL +
				"SENSE " + (code instanceof G02 ? "CW" : "CCW") + NL;
	}

	private String params(Params p, int mask) {
		return ((mask & NO_FEEDRATE) == 0 ? ("FEEDRATE " + IFMT.format(p.getFeedrate_mm_min()) + NL) : "") +
				"TOOL-NO " + p.getToolId() + NL +
				"SPINDLE-SPEED " + IFMT.format(p.getSpindleSpeedRPM()) + NL;
	}

	private String params(Params p) {
		return params(p, 0);
	}

	private String xz(XZ code) {
		return "X " + FFMT.format(code.getX()) + NL + 
				"Z " + FFMT.format(code.getZ()) + NL;  
	}

	public Program receive(SerialPort port) throws SerialPortException {
		System.out.println("Waiting for header...");
		while(read(port) != START_OF_HEADER) {}

		System.out.println("Reading page...");
		while(readPage(port)) {}

		System.out.println("done.");

		return null;
	}

	private int read(SerialPort port) throws SerialPortException {
		return port.readBytes(1)[0];
	}

	private boolean readPage(SerialPort port) throws SerialPortException {
		for(;;) {
			int c = read(port);
			switch(c) {
			case END_OF_TRANSMISSION:
				return false;
			case END_OF_TEXT:
				System.out.println("--------------------------");
				return true;
			case START_OF_TEXT:
				continue;
			default:
				System.out.print((char)c + "[" +c + "]");
				System.out.flush();
			}
		}
	}

	@Override
	public String toString() {
		return "Denford Orac";
	}

	@Override
	public void send(String portName, Rate rate, DataModel db, Operation ... operations) throws Throwable {
		send(portName, rate, toProgram(db, operations));
	}

	public void send(String portName, Rate rate, Program prog) throws Throwable {
		ByteArrayOutputStream file = new ByteArrayOutputStream();
		file.write(START_OF_HEADER);
		int pageNo = 1;
		for(GCode code : prog.getFlatCode()) {
			writePage(file, pageNo, code);
			pageNo++;
		}
		file.write(END_OF_TRANSMISSION);
		file.close();

		Window       parent     = FocusManager.getCurrentManager().getActiveWindow();
		Throwable[]  error      = new Throwable[1];
		JDialog      dlgUI      = new JDialog(parent, "Sending to Orac" , ModalityType.APPLICATION_MODAL);
		JProgressBar progressUI = new JProgressBar(0, file.size());
		JLabel       messageUI  = new JLabel("Connecting to Orac...");
		JPanel       panelUI    = new JPanel(new BorderLayout());
		panelUI.setLayout(new BorderLayout(15, 15));
		panelUI.setBorder(new EmptyBorder(10, 10, 10, 10));
		panelUI.add(BorderLayout.CENTER, messageUI);
		panelUI.add(BorderLayout.SOUTH,  progressUI);
		panelUI.add(BorderLayout.NORTH,  new JLabel("Communicating over " + portName + " at " + rate));
		dlgUI.add(panelUI);
		dlgUI.pack();
		dlgUI.setSize(dlgUI.getSize().width+50, dlgUI.getSize().height);
		dlgUI.setLocationRelativeTo(parent);
		SerialPort port = new SerialPort(portName);
		port.openPort();
		port.setParams(rate.rate(), 8, 1, 0);

		new Thread(()->{
			try {
				System.out.println(new String(file.toByteArray()));
				progressUI.setIndeterminate(true);
				messageUI.setText("Waiting for Orac...");
				while(!(port.isDSR())) {}
				messageUI.setText("Sending program...");
				progressUI.setIndeterminate(false);
				new Thread(new Runnable() {
					@Override
					public void run() {
						for(int i = 0; i < file.size(); i += 10) {
							progressUI.setString(i + " of " + file.size() + " bytes");
							progressUI.setValue(i);
							try {
								Thread.sleep(100000/rate.rate());
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					}
				}).start();
				port.writeBytes(file.toByteArray());
				messageUI.setText("Done.");
				port.setDTR(false);
				dlgUI.dispose();
			} catch(Throwable t) {
				error[0] = t;
			}
		}).start();
		dlgUI.setVisible(true);
		if(error[0] != null) throw error[0];
		port.closePort();
	}

	private GCode nop() {
		return new Program();
	}

	private XZ xzRel(double length, double radius, double delta, Params params) {
		return radius == 0 ? new G01(delta, length, params) : (radius > 0 ? new G02(delta, length, radius, params) : new G03(delta, length, -radius, params));
	}

	private GCode cut(DataModel db, Tool tool, double depthLeft, double depthRight, double length, double radius, Params cut, Params move) {
		double depth = Math.max(depthLeft, depthRight);
		double delta = depthRight - depthLeft;
		if(depth <= 0) return nop();
		if(depth <= db.cutDepth_mm())
			if(tool.getCut() == Cut.right) {
				return new Program(
						xzRel(-length, -radius, 0, move),
						new G00(-depthLeft, 0, cut),
						xzRel(+length, +radius, -delta, cut),
						new G00(depthRight, 0, move));
			} else {
				return new Program(
						new G00(-depthRight, 0,        cut),
						xzRel(-length, +radius, delta, cut),
						xzRel(+length, -radius, depthLeft, move));
			}
		else {
			Count  count = new Count(db, depth);
			if(tool.getCut() == Cut.right) {
				return new Program(
						new G00((delta < 0 ? -delta : 0), 0, move),
						new Loop(count,
								new G00(+count.step, 0, move),
								xzRel(-length, +radius, +delta, move),
								new G00(-2*count.step, 0, cut),
								xzRel(+length, -radius, -delta, cut)),
						new G00(depthLeft+delta, 0, move)
						);
			} else {
				return new Program(
						new G00((delta < 0 ? -delta : 0)+count.step, 0, move),
						new Loop(count,
								new G00(-2*count.step, 0, cut),
								xzRel(-length, +radius, +delta, cut),
								new G00(+count.step, 0, move),
								xzRel(+length, -radius, -delta, move)),
						new G00(depthLeft+delta-count.step, 0, move)
						);
			}
		}
	}

	private GCode thread(DataModel db, double inOutD, double rootD, double length, double pitch, Params cut, Params move) {
		return new Program(
				new G00(db.getMaterial().getOD()-inOutD, 0, cut.spindleSpeed(G33.calcSpeed(pitch))),
				new G33(inOutD, rootD, db.cutDepth_mm(), length, pitch, cut),
				new G00(inOutD-db.getMaterial().getOD(), length, move));
	}

	private GCode bore(DataModel db, double depthLeft, double depthRight, double length, double radius, Params cut, Params move, double toolDiameter) {
		double moR       = db.getMaterial().getOD() / 2;
		double toolR     = toolDiameter / 2;
		double depth     = (moR-Math.min(depthLeft, depthRight))-toolR;
		int count        = Math.max(1, (int)((depth + db.cutDepth_mm()) / db.cutDepth_mm()));
		double cutdLeft  = ((moR-depthLeft) -toolR) / count;
		double cutdRight = ((moR-depthRight)-toolR) / count;

		double[] left  = new double[count+1];
		double[] right = new double[count+1];

		for(int i = 0; i <= count; i++) {
			left[i]  = toolR-i*cutdLeft;
			right[i] = toolR-i*cutdRight;
		}		

		Program result = new Program();
		result.add(new G00(-(moR-toolR), 0, move));
		for(int i = 1; i <= count; i++) {
			result.add(xzRel(-length, radius, right[i-1]-left[i], cut));
			result.add(xzRel(+length, radius, left[i]-right[i], cut));
		}
		result.add(new G00(+depthRight, 0, move));
		return result;
	}

	private GCode trepan(DataModel db, double od, double id, double depth, Params cut, Params move, double toolDiameter) {
		Count      count = new Count(db, depth);
		OpMaterial m     = db.getMaterial();

		od -= toolDiameter * 2;
		
		return new Program(
				new G00((m.getOD()-od)/-2, 0, move),
				new Loop(count,
						new G00(0,          -count.step, cut),
						new G00(-(od-id)/2, 0,           cut),
						new G00(+(od-id)/2, 0,           move)),
				new G00(0, depth,                        move),
				new G00(-((m.getOD()-od)/-2), 0,         move)
				);
	}

	@Override
	public Program toProgram(DataModel db, Operation ... operations) {
		Program       result = new Program();
		OpMaterial    m      = db.getMaterial();
		List<Program> subs   = new ArrayList<>();

		Params moveBase = new Params(db.getDriver().getMaxFeedrate_mm_min(), 0, db.speedRPM(db.getMaterial().getOD(), Speed.STRAIGHT_AVG));
		Params cutBase  = new Params(db.feedRate_mm_s(),                   0,   db.speedRPM(db.getMaterial().getOD(), Speed.STRAIGHT_AVG));

		// always mm
		result.add(new G71());
		// always incremental
		result.add(new G91());

		boolean moveOutBeforeToolChange = true;
		int lastToolId                  = -1;
		for(Operation op : operations) {
			int toolId = op.getToolId();
			Params move   = moveBase.tool(toolId);
			Params cut    = cutBase.tool(toolId);
			Params groove = cutBase.tool(toolId).feedRate(cut.getFeedrate_mm_min() / 3);
			if(!(op instanceof OpDriver) && !(op instanceof OpMaterial) && toolId != lastToolId) {
				if(moveOutBeforeToolChange)
					result.add(new G00(+getSafeDist_mm(), 0, move.tool(lastToolId)));
				result.add(new G00(-getSafeDist_mm(), 0, move));
				lastToolId              = toolId;
				moveOutBeforeToolChange = true;
			}
			if(op.skip()) {
				result.add(new G00(0, -op.getTotalLength(), move.tool(lastToolId)));
			} else if(op instanceof OpDriver) {
				continue;
			} else if(op instanceof OpMaterial) {
				// set datum
				result.add(new G50(m.getOD()/2+getSafeDist_mm(), 0));
				// turn on spindle
				result.add(new M03());
				moveOutBeforeToolChange = false;
			} else if(op instanceof OpFace) {
				OpFace o     = (OpFace)op;
				Count    count = new Count(db, o.getLength());
				result.add(new G00(0, o.getLength(), cut));
				result.add(new Loop(count,
						new G00(-m.getOD()/2, 0,          cut),
						new G00(+m.getOD()/2, 0,          cut),
						new G00(0,           -count.step, move)
						));
			} else if(op instanceof ICutLinear) {
				ICutLinear o = (ICutLinear)op;
				double mod = m.getOD();
				for(int i = 0; i < o.getN(); i++) {
					if(op instanceof OpBore) {
						result.add(bore(db, (mod-o.getLeftD()) / 2, (mod-o.getRightD()) / 2, o.getLength(), 0, cut, move, o.getTool().getWidth()));
					} else if(op instanceof OpThreadExt) { 
						OpThreadExt ot = (OpThreadExt)op;
						beginOffset(db, result, o, move);
						result.add(thread(db, ot.getOD(), ot.getID(), ot.getCutLength(), ot.getPitch(), cut, move));
						endOffset(db, result, o, move);
					} else {
						if((o.getOffset() > 0 || o.getTool().getCut() == Cut.both) && !(o instanceof OpChamferLeft || o instanceof OpChamferRight)) {
							beginOffset(db, result, o, move);
							result.add(cut(db, o.getTool(), (mod - o.getLeftD()) / 2, (mod - o.getRightD()) / 2, o.getLength()-(o.getTool().getWidth()+o.getOffset()), 0, cut, move));
							endOffset(db, result, o, move);
						} else {
							if(o instanceof OpChamferLeft)
								beginOffset(db, result, o, move);
							result.add(cut(db, o.getTool(), (mod - o.getLeftD()) / 2, (mod - o.getRightD()) / 2, o.getLength(),                                        0, cut, move));
							if(o instanceof OpChamferLeft)
								endOffset(db, result, o, move);
							else
								result.add(new G00(0, -o.getLength(), move));
						}
					}
				}
			} else if(op instanceof OpTrepan) {
				OpTrepan o      = (OpTrepan)op;
				result.add(trepan(db, o.getOD(), o.getID(), o.getLength(), cut, move, o.getTool().getWidth()));
			} else if(op instanceof OpGroove) {
				OpGroove o      = (OpGroove)op;
				for(int i = 0; i < o.getN(); i++) {
					beginOffset(db, result, o, move);
					result.add(new G00(-(m.getOD()-o.getOD())/2, 0, groove));
					result.add(new G00(+(m.getOD()-o.getOD())/2, 0, move));								
					endOffset(db, result, o, move);
				}
			} else if(op instanceof OpPart) {
				OpPart     o      = (OpPart)op;
				beginOffset(db, result, o, move);
				result.add(new G00(-(m.getOD()+0.5)/2, 0, groove));
				result.add(new G00(+(m.getOD()+0.5)/2, 0, move));								
				endOffset(db, result, o, move);
			} else if(op instanceof OpCutCircleLeft) {
				OpCutCircleLeft o = (OpCutCircleLeft)op;
				if(o.getTool().getCut() != Cut.left)
					beginOffset(db, result, o, move);					
				result.add(cut(db, o.getTool(), ((m.getOD() - o.getOD())) / 2 + o.getLength(), (m.getOD() - o.getOD()) / 2, o.getLength(), o.getLength(), cut, move));
				if(o.getTool().getCut() != Cut.left)
					endOffset(db, result, o, move);
				else
					result.add(new G00(0, -o.getLength(), move));
			} else if(op instanceof OpCutCircleRight) {
				OpCutCircleRight o = (OpCutCircleRight)op;
				result.add(cut(db, o.getTool(), (m.getOD() - o.getOD()) / 2, ((m.getOD() - o.getOD()))/ 2 + o.getLength(), o.getLength(), o.getLength(), cut, move));
				result.add(new G00(0, -o.getLength(), move));
			} else {
				db.error(new IOException("Orac: unsupported operation: " + op));
			}
		}

		result.add(new M05());
		result.add(new G00(0, db.getOffset(null), moveBase.tool(lastToolId)));
		result.add(new M02());

		for(Program sub : subs)
			result.add(sub);

		return result;
	}

	private void beginOffset(DataModel db, Program result, IOffset o, Params move) {
		result.add(new G00(0, -(o.getOffset()+o.getTool().getWidth()), move));
	}

	private void endOffset(DataModel db, Program result, IOffset o, Params move) {
		result.add(new G00(0, (o.getOffset()+o.getTool().getWidth())-o.getLength(), move));
	}

	@Override
	public double maxOD_mm() {
		return 115;
	}

	@Override
	public double maxLength_mm() {
		return 400;
	}

	@Override
	public double maxRPM() {
		return 2000;
	}

	@Override
	public double minRPM() {
		return 40;
	}

	@Override
	public double resolution_mm() {
		return 0.01;
	}

	@Override
	public int maxGCodes() {
		return 99;
	}

	@Override
	public double getMaxFeedrate_mm_min() {
		return 1200;
	}

	@Override
	public double getMinFeedrate_mm_min() {
		return 0;
	}

	@Override
	public double getMaxCut_mm() {
		return 5;
	}

	@Override
	public double getSafeDist_mm() {
		return 20;
	}

	@Override
	public int numToolSlots() {
		return 8;
	}
}
