package ch.bluecc.jcam.driver;

import java.util.EnumSet;

import ch.bluecc.jcam.gcode.Program;
import ch.bluecc.jcam.turn.DataModel;
import ch.bluecc.jcam.turn.Operation;
import jssc.SerialPort;

public abstract class Driver {
	public enum Rate {RATE_300(SerialPort.BAUDRATE_300), RATE_1200(SerialPort.BAUDRATE_1200), RATE_2400(2400);
		private int rate;

		private Rate(int rate) {this.rate = rate;}

		public int rate() {
			return rate;
		}}

	public abstract String[] getPorts();
	public abstract EnumSet<Rate> getRates();	
	public abstract void    send(String port, Rate rate, DataModel db, Operation ... operations)  throws Throwable;
	public abstract double  maxOD_mm();
	public abstract double  maxLength_mm();
	public abstract double  maxRPM();
	public abstract double  minRPM();
	public abstract double  resolution_mm();
	public abstract Program toProgram(DataModel db, Operation ... operations);
	public abstract int     maxGCodes();
	public abstract double  getMaxFeedrate_mm_min();
	public abstract double  getMinFeedrate_mm_min();
	public abstract double  getMaxCut_mm();
	public abstract double  getSafeDist_mm();
	public abstract int     numToolSlots();
}
