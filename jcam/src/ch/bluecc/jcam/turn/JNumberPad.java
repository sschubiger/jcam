package ch.bluecc.jcam.turn;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.plaf.basic.BasicBorders;
import javax.swing.text.JTextComponent;

@SuppressWarnings("nls")
public class JNumberPad extends JPanel implements MouseListener {
	private static final long   serialVersionUID = -157662117285667220L;
	private static final String KEYS             = "789N456P123E0.-D";

	public JNumberPad() {
		setLayout(new GridBagLayout());
		for(int i = 0; i < KEYS.length(); i++) {
			JLabel button = new JLabel("   " + KEYS.charAt(i) + "   ");
			add(button, new GridBagConstraints(i % 4, i / 4, 1, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(2, 2, 2, 2), 0, 0));
			button.setFont(button.getFont().deriveFont(UI.FONT_SIZE * 2));
			button.setBorder(BasicBorders.getButtonBorder());
			button.addMouseListener(this);
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		JLabel label  = (JLabel)e.getSource();
		label.setBackground(Color.YELLOW);
		label.setForeground(Color.YELLOW);
		String action = label.getText().trim();
		KeyboardFocusManager focus = KeyboardFocusManager.getCurrentKeyboardFocusManager();
		switch(action.charAt(0)) {
		case 'N':
		case 'E':
			focus.focusNextComponent();
			break;
		case 'P':
			focus.focusPreviousComponent();
			break;
		case 'D':
			Component focusCmp = focus.getFocusOwner();
			if(focusCmp instanceof JTextComponent)
				((JTextComponent)focusCmp).setText("");
			break;
		default:
			Component c = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
			c.dispatchEvent(new KeyEvent(c, KeyEvent.KEY_TYPED, e.getWhen(), 0, KeyEvent.VK_UNDEFINED, action.charAt(0)));
			break;
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {}

	@Override
	public void mouseReleased(MouseEvent e) {
		JLabel label  = (JLabel)e.getSource();
		label.setBackground(null);
		label.setForeground(null);
	}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}
}
