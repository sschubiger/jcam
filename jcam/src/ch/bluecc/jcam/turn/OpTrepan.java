package ch.bluecc.jcam.turn;

import java.awt.Graphics2D;

import javax.swing.JPanel;
import javax.swing.JSpinner;

@SuppressWarnings("nls")
public class OpTrepan extends Operation implements IOffset {
	private JSpinner        odUI;
	private JSpinner        idUI;
	private JSpinner        lenghtUI;

	public OpTrepan(DataModel db) {
		super(db);
	}

	@Override
	protected void createChildren(JPanel parent) {
		createActions(parent);
		createTools(parent);
		odUI     = createOD(parent);
		idUI     = createID(parent);
		lenghtUI = createLength("Depth", parent, 0);
	}

	@Override
	public void draw(Graphics2D g2) {
		double     toolW = getTool().getWidth();  
		warning(toolW > val(odUI) - val(idUI) ? "Tool width ("+toolW+") > trepan width" : null);	
		OpMaterial mat   = db.getMaterial();
		double off = db.getOffset(this);
		trepan(g2, mat.getOD(), val(odUI), val(idUI), off, val(lenghtUI));  
	}

	@Override
	public String toString() {
		return "Trepanning";
	}

	@Override
	public double getLength() {
		return val(lenghtUI);
	}

	public double getOD() {
		return val(odUI);
	}

	public double getID() {
		return val(idUI);
	}

	@Override
	public double getTotalLength() {
		return 0;
	}

	@Override
	public double getOffset() {
		return 0;
	}	
}
