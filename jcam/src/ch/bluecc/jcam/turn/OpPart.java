package ch.bluecc.jcam.turn;

import java.awt.Graphics2D;

import javax.swing.JPanel;
import javax.swing.JSpinner;

@SuppressWarnings("nls")
public class OpPart extends Operation implements IOffset {
	private JSpinner        offsetUI;

	public OpPart(DataModel db) {
		super(db);
	}

	@Override
	protected void createChildren(JPanel parent) {
		createActions(parent);
		createTools(parent);
		offsetUI = createOffset(parent);
	}

	@Override
	public void draw(Graphics2D g2) {
		OpMaterial mat = db.getMaterial();
		cut(g2, mat.getOD(), 0, 0, db.getOffset(this) + getOffset(), getTool().getWidth());
	}

	@Override
	public double getLength() {
		return getTool().getWidth() + val(offsetUI);
	}

	@Override
	public double getTotalLength() {
		return getLength();
	}

	@Override
	public String toString() {
		return "Parting";
	}

	@Override
	public double getOffset() {
		return val(offsetUI);
	}
}
