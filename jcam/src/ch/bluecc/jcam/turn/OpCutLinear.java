package ch.bluecc.jcam.turn;

import java.awt.Graphics2D;

import javax.swing.JPanel;
import javax.swing.JSpinner;

@SuppressWarnings("nls")
public class OpCutLinear extends Operation  implements ICutLinear  {
	private JSpinner        nUI;
	private JSpinner        lodUI;
	private JSpinner        rodUI;
	private JSpinner        lenghtUI;

	public OpCutLinear(DataModel db) {
		super(db);
	}

	@Override
	protected void createChildren(JPanel parent) {
		createActions(parent);
		createTools(parent);
		nUI      = createN(parent);
		lodUI    = createOD("Left ",  parent);
		rodUI    = createOD("Right ", parent);
		lenghtUI = createLength(parent, 0);
	}

	@Override
	public void draw(Graphics2D g2) {
		double     toolW = getTool().getWidth();  
		warning(toolW > val(lenghtUI) ? "Tool width ("+toolW+") > length" : null);	
		OpMaterial mat   = db.getMaterial();
		double off = db.getOffset(this);
		for(int i = 0; i < val(nUI); i++, off += val(lenghtUI))
			cut(g2, mat.getOD(), val(lodUI), val(rodUI), off, val(lenghtUI));  
	}

	@Override
	public String toString() {
		return "Cutting (linear)";
	}

	@Override
	public double getLength() {
		return val(lenghtUI);
	}
	
	@Override
	public double getTotalLength() {
		return getLength() * val(nUI);
	}

	@Override
	public double getLeftD() {
		return val(lodUI);
	}

	@Override
	public double getRightD() {
		return val(rodUI);
	}

	@Override
	public double getOffset() {
		return 0;
	}

	@Override
	public int getN() {
		return (int) val(nUI);
	}
}
