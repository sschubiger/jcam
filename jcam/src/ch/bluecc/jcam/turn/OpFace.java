package ch.bluecc.jcam.turn;

import java.awt.Graphics2D;

import javax.swing.JPanel;
import javax.swing.JSpinner;

@SuppressWarnings("nls")
public class OpFace extends Operation {
	private JSpinner lenghtUI;
	
	public OpFace(DataModel db) {
		super(db);
	}

	@Override
	protected void createChildren(JPanel parent) {
		createActions(parent);
		createTools(parent);
		lenghtUI = createLength(parent, 0);
	}
	
	@Override
	public String toString() {
		return "Facing";
	}
	
	@Override
	public void draw(Graphics2D g2) {
		OpMaterial mat = db.getMaterial();
		cut(g2, mat.getOD(), mat.getOD(), mat.getOD(), -val(lenghtUI), val(lenghtUI)); 
	}

	@Override
	public double getTotalLength() {
		return 0;
	}

	@Override
	public double getLength() {
		return val(lenghtUI);
	}
}
