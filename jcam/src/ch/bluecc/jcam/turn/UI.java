package ch.bluecc.jcam.turn;

import java.awt.Component;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

public class UI {
	public static float FONT_SIZE = 22;

	public static <T extends JComponent> T configure(T result) {
		result.setFont(result.getFont().deriveFont(UI.FONT_SIZE));
		return result;
	}

	public static class JLabelRenderer implements ListCellRenderer<Object> {
		private int hPos = JLabel.CENTER;
		private int vPos = JLabel.CENTER;

		public JLabelRenderer(int horizontalPosition, int verticalPosition) {
			this.vPos = verticalPosition;
			this.hPos = horizontalPosition;
		}

		@Override
		public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
			JLabel result = (JLabel)value;
			if(result != null) {
				result.setHorizontalTextPosition(hPos);
				result.setVerticalTextPosition(vPos);
			} else {
				result = new JLabel("???"); //$NON-NLS-1$
			}
			return result;
		}
	}

	public static String toString(Object o) {
		return o instanceof JLabel ? ((JLabel)o).getText() : (o == null ? "null" : o.toString()); //$NON-NLS-1$
	}

	public static boolean matches(String value, JComboBox<?> ui, int idx) {
		return value.equals(toString(ui.getItemAt(idx))); 
	}

	public static JComboBox<JLabel> createTools(Tool[] tools, JComponent parent, Object constraints) {
		JComboBox<JLabel> toolUI = new JComboBox<>(createToolItems(tools));
		toolUI.setEditable(false);
		toolUI.setRenderer(new JLabelRenderer(JLabel.CENTER, JLabel.BOTTOM));
		parent.add(toolUI, constraints);
		return toolUI;
	}

	public static JLabel[] createToolItems(Tool[] tools) {
		JLabel[] items = new JLabel[tools.length];
		for(int i = 0; i < tools.length; i++)
			items[i] = new JLabel(tools[i].toString(), tools[i].getImage(), JLabel.LEFT);
		return items;
	}
}
