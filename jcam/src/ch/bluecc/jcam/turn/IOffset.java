package ch.bluecc.jcam.turn;

public interface IOffset {
	Tool   getTool();
	double getOffset();
	double getTotalLength();
	double getLength();
}
