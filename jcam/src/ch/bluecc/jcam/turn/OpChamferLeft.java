package ch.bluecc.jcam.turn;

import java.awt.Graphics2D;

import javax.swing.JPanel;
import javax.swing.JSpinner;

@SuppressWarnings("nls")
public class OpChamferLeft extends Operation implements ICutLinear {
	private JSpinner        odUI;
	private JSpinner        lenghtUI;
	
	public OpChamferLeft(DataModel db) {
		super(db);
	}

	@Override
	protected void createChildren(JPanel parent) {
		createActions(parent);
		createTools(parent);
		odUI     = createOD(parent);
		lenghtUI = createLength(parent, 0);
	}

	@Override
	public void draw(Graphics2D g2) {
		double     toolW = getTool().getWidth();  
		warning(toolW > val(lenghtUI) ? "Tool width ("+toolW+") > length" : null);	
		OpMaterial mat   = db.getMaterial();
		cut(g2, mat.getOD(), val(odUI)-getLength()*2, val(odUI), db.getOffset(this), getLength());  
	}

	@Override
	public String toString() {
		return "Left chamfering";
	}

	@Override
	public double getLength() {
		return val(lenghtUI);
	}
	
	@Override
	public double getTotalLength() {
		return getLength();
	}

	public double getOD() {
		return val(odUI);
	}

	@Override
	public int getN() {
		return 1;
	}

	@Override
	public double getOffset() {
		return 0;
	}

	@Override
	public double getLeftD() {
		return getRightD()-getLength()*2;
	}

	@Override
	public double getRightD() {
		return getOD();
	}
}
