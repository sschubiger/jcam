package ch.bluecc.jcam.turn;

import java.awt.Graphics2D;
import java.awt.geom.GeneralPath;

import javax.swing.JPanel;
import javax.swing.JSpinner;

@SuppressWarnings("nls")
public class OpCutCircleLeft extends Operation implements IOffset {
	private static final int N_POINTS = 10;
	
	private JSpinner odUI; 
	private JSpinner lengthUI;
	
	public OpCutCircleLeft(DataModel db) {
		super(db);
	}

	@Override
	protected void createChildren(JPanel parent) {
		createActions(parent);
		createTools(parent);

		odUI     = createOD(parent);
		lengthUI = createLength(parent, 0);
	}

	@Override
	public void draw(Graphics2D g2) {
		double     length = getLength();
		double     od     = val(odUI) / 2 - length;
		OpMaterial mat    = db.getMaterial();
		double     off    = db.getOffset(this);
		
		GeneralPath top = new GeneralPath();
		top.moveTo(off, mat.getOD()/2);
		for(int i = 0; i <= N_POINTS; i++) {
			double a = (Math.PI * 0.5 * i) / N_POINTS;
			top.lineTo(off + length * Math.sin(a), od + length * Math.cos(a));
		}
		top.lineTo(off+length, mat.getOD()/2);
		top.lineTo(off,        mat.getOD()/2);
		top.closePath();

		GeneralPath bottom = new GeneralPath();
		bottom.moveTo(off, -mat.getOD()/2);
		for(int i = 0; i <= N_POINTS; i++) {
			double a = (Math.PI * 0.5 * i) / N_POINTS;
			bottom.lineTo(off + length * Math.sin(a), -od + length * -Math.cos(a));
		}
		bottom.lineTo(off+length, -mat.getOD()/2);
		bottom.lineTo(off,        -mat.getOD()/2);
		bottom.closePath();

		g2.setColor(getCutColor());
		g2.fill(top);
		g2.fill(bottom);
	}

	@Override
	public double getTotalLength() {
		return getLength();
	}

	@Override
	public double getLength() {
		return val(lengthUI);
	}

	@Override
	public String toString() {
		return "Left quarter circle";
	}

	public double getOD() {
		return val(odUI);
	}

	@Override
	public double getOffset() {
		return 0;
	}
}
