package ch.bluecc.jcam.turn;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;

import ch.bluecc.jcam.gcode.G00;
import ch.bluecc.jcam.gcode.G01;
import ch.bluecc.jcam.gcode.G02;
import ch.bluecc.jcam.gcode.G02_03;
import ch.bluecc.jcam.gcode.G06;
import ch.bluecc.jcam.gcode.G33;
import ch.bluecc.jcam.gcode.G50;
import ch.bluecc.jcam.gcode.G71;
import ch.bluecc.jcam.gcode.G73;
import ch.bluecc.jcam.gcode.G90;
import ch.bluecc.jcam.gcode.G91;
import ch.bluecc.jcam.gcode.GCode;
import ch.bluecc.jcam.gcode.IParams;
import ch.bluecc.jcam.gcode.M02;
import ch.bluecc.jcam.gcode.M03;
import ch.bluecc.jcam.gcode.M05;
import ch.bluecc.jcam.gcode.Program;

@SuppressWarnings("nls")
public class Preview extends JComponent implements MouseWheelListener {
	private static final long serialVersionUID = 7438786136075411628L;
	private static final DecimalFormat FMT = new DecimalFormat("#.##mm");

	private DataModel db;

	private static final Color[] COLORS = {
			Color.GRAY,
			Color.RED,	
			Color.GREEN,	
			Color.BLUE,	
			Color.CYAN,	
			Color.MAGENTA,	
			Color.YELLOW,	
			Color.ORANGE,
			Color.PINK,
	};

	private static final int     N_CIRCLE = 40;
	private static final double  EPSILON  = 0.001;
	private static final double  PI2      = Math.PI*2;
	private List<GeneralPath>    paths    = new ArrayList<>();
	private List<Color>          colors   = new ArrayList<>();
	private float                scale    = 1;
	

	public Preview(DataModel db) {
		this.db = db;
		addMouseWheelListener(this);
	}

	private double r(double v) {
		double res = db.getDriver().getResolution_mm();
		v *= (int) (1.0 / res);
		return Math.round(v) * res;
	}
	
	public void setProgram(Program prog) {
		paths.clear();
		colors.clear();
		if(prog != null) {
			GeneralPath current = new GeneralPath();
			boolean     abs     = true;
			int         toolId  = 0;
			float       scale   = 1;
			int         count   = -1;
			int         loopPc  = -1;
			List<GCode> gcodes = prog.getFlatCode();
			for(int pc = 0; pc < gcodes.size(); pc++) {
				Point2D p = current.getCurrentPoint();
				GCode gcode = gcodes.get(pc);
				if(gcode instanceof IParams) {
					if(((IParams)gcode).getParams().getToolId() != toolId) {
						paths.add(current);
						colors.add(COLORS[toolId]);
						toolId = ((IParams)gcode).getParams().getToolId();
						current = new GeneralPath();
						current.moveTo(p.getX(), p.getY());
					}
				}
				if(gcode instanceof G00) {
					G00 g00 = (G00)gcode;
					if(abs) current.lineTo(           -scale*r(g00.getZ()),            scale*r(g00.getX()));
					else	current.lineTo(p.getX() + -scale*r(g00.getZ()), p.getY() + scale*r(g00.getX()));
				} else if(gcode instanceof G01) {
					G01 g01 = (G01)gcode;
					if(abs) current.lineTo(           -scale*r(g01.getZ()),            scale*r(g01.getX()));
					else	current.lineTo(p.getX() + -scale*r(g01.getZ()), p.getY() + scale*r(g01.getX()));
				} else if(gcode instanceof G02_03) {
					G02_03 g02_03 = (G02_03)gcode;
					double r  = g02_03.getRadius();
					try {
						double[] ps = computeCircleParams(scale, p, g02_03, abs);
						if(g02_03 instanceof G02) { // clockwise
							for(int i = 0; i <= N_CIRCLE; i++) {
								double a = (i*PI2)/N_CIRCLE;
								if(a > ps[2] && a < ps[3]) {
									double x = ps[0] + r*-Math.sin(a);
									double y = ps[1] + r*-Math.cos(a);
									current.lineTo(x, y);
								}
							}
						} else { // counter clockwise
							for(int i = 0; i <= N_CIRCLE; i++) {
								double a = ((N_CIRCLE-i)*PI2)/N_CIRCLE;
								if(a > ps[2] && a < ps[3]) {
									double x = ps[0] + r*-Math.sin(a);
									double y = ps[1] + r*-Math.cos(a);
									current.lineTo(x, y);
								}
							}
						}
					} catch(Throwable t) {
						db.error(t);
					}
					if(abs) current.lineTo(           -scale*r(g02_03.getZ()),            scale*r(g02_03.getX()));
					else	current.lineTo(p.getX() + -scale*r(g02_03.getZ()), p.getY() + scale*r(g02_03.getX()));
				} else if(gcode instanceof G06) {if(--count > 0) pc = loopPc;
				} else if(gcode instanceof G33) {
					G33 g33 = (G33)gcode;
					double pitch = g33.getPitch();
					double depth = (g33.getInOutDiam()-g33.getX())/2;
					for(double y = g33.getInOutDiam()/2; y > g33.getX()/2; y -= g33.getCutInc()) {
						current.lineTo(p.getX(), scale*r((y+depth)));
						for(double x = 0; x < g33.getZ(); x += pitch) {
							current.lineTo(p.getX() + scale*r(x),           scale*r(y+depth));
							current.lineTo(p.getX() + scale*r(x + pitch/2), scale*r(y));
						}
						current.lineTo(p.getX()+scale*r(g33.getZ()),  scale*r(y+depth));						
						current.lineTo(p.getX(),                      scale*r(y+depth));
					}
					current.lineTo(p.getX() + scale*g33.getZ(), p.getY());
				} else if(gcode instanceof G50) {current.moveTo(-scale*r(((G50)gcode).getZ()), scale*r(((G50)gcode).getX()));
				} else if(gcode instanceof G71) {scale  = 1;
				} else if(gcode instanceof G73) {loopPc = pc; count = ((G73)gcode).getCount();
				} else if(gcode instanceof G90) {abs = true;
				} else if(gcode instanceof G91) {abs = false;
				} else if(gcode instanceof M02) {
				} else if(gcode instanceof M03) {
				} else if(gcode instanceof M05) {
				} else db.error(new IOException("Unsupported GCode: " + gcode.getClass().getName()));
			}
			paths.add(current);
			colors.add(COLORS[toolId]);
		}
		repaint();
	}

	private double[] computeCircleParams(double scale, Point2D start, G02_03 gcode, boolean abs) {
		Point2D end    = new Point2D.Double(-scale*(gcode.getZ())+(abs?0:start.getX()), scale*(gcode.getX())+(abs?0:start.getY()));
		Point2D center = arcCenter(start, end, gcode.getRadius(), gcode instanceof G02);
		double  aStart = Math.PI+Math.atan2(start.getX()-center.getX(), start.getY()-center.getY());
		double  aEnd   = Math.PI+Math.atan2(end.getX()  -center.getX(), end.getY()  -center.getY());

		if(aStart > aEnd) {
			double tmp = aEnd;
			aEnd = aStart;
			aStart = tmp;
		}

		return new double[] {
				center.getX(), center.getY(),
				aStart, aEnd,
		};
	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D)g;

		Rectangle bounds = getBounds();
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, bounds.width, bounds.height);

		OpMaterial mat = db.getMaterial();
		if(mat == null) return;

		g2.translate(bounds.getWidth() / 2, bounds.height / 2);
		float hscale = (float)((bounds.getWidth()  / mat.getTotalLength()) * 0.9);
		float vscale = (float)((bounds.getHeight() / (mat.getOD()+2*db.getDriver().getSafeDist())) * 0.9);
		float scale  = Math.min(hscale, vscale);

		Shape[] measurments = new Shape[db.getOperationsList().length*2+1];
		double ystep = 20 / scale;
		double yoff  = -(mat.getOD() / 2 + ystep);
		double xoff  = 0;
		int    i     = 0;
		for(Operation op : db.getOperationsList()) {
			if(op instanceof OpFace || op instanceof OpMaterial || op instanceof OpDriver)
				xoff = 0;
			else {
				xoff += op.getTotalLength();
				yoff -= ystep;
				measurments[i++] = new Line2D.Double(0, yoff, xoff, yoff);
				measurments[i++] = new Line2D.Double(xoff, yoff, xoff, 0);
			}
		}
		measurments[i++] = new Line2D.Double(0, yoff, 0, 0);

		vscale = (float)((bounds.getHeight() / (mat.getOD() - yoff)) * 0.9);
		scale  = Math.min(hscale, vscale) * this.scale;
		g2.scale(-scale, scale);
		g2.translate(mat.getTotalLength()/-2, 0);
		for(Operation op : db.getOperationsList())
			op.draw(g2);

		g2.setColor(Color.BLUE);
		g2.setStroke(new BasicStroke(1/scale, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{9/scale,9/scale,1/scale,9/scale}, 0));
		g2.drawLine(-bounds.width, 0, bounds.width, 0);
		g2.setColor(Color.YELLOW);
		g2.setStroke(new BasicStroke(1/scale, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, null, 0));
		double r = 10/scale;
		g2.draw(new Arc2D.Double(-r, -r+mat.getOD()/2, 2*r, 2*r, 0,  360, Arc2D.CHORD));
		g2.fill(new Arc2D.Double(-r, -r+mat.getOD()/2, 2*r, 2*r, 0,   90, Arc2D.PIE));
		g2.fill(new Arc2D.Double(-r, -r+mat.getOD()/2, 2*r, 2*r, 180, 90, Arc2D.PIE));
		g2.setFont(g2.getFont().deriveFont(AffineTransform.getScaleInstance(-1/scale, 1/scale)));
		i = 0;
		for(Shape s : measurments) {
			if(s != null) {
				g2.draw(s);
				if((i & 1) == 0) {
					Rectangle2D bb = s.getBounds2D();
					String txt = FMT.format(bb.getWidth());
					float x = (float)(bb.getX()+bb.getWidth()/2);
					float y = (float)(bb.getY()-2/scale);
					float xinc = 8 / scale;
					for(int j = 0; j < txt.length(); j++) {
						g2.drawString(txt.substring(j, j+1), x, y);
						x -= xinc;
					}
				}
				i++;
			}
		}
		double[] coords = new double[6];
		int numPaths = 0;
		for(i = 0; i < paths.size(); i++) {
			GeneralPath path = paths.get(i);
			double rr = 0;
			g2.setColor(colors.get(i));
			g2.draw(path);
			for(PathIterator pi = path.getPathIterator(null); !(pi.isDone());) {
				switch(pi.currentSegment(coords)) {
				case PathIterator.SEG_MOVETO:
					rr = 2*r;
					g2.fill(new Arc2D.Double(coords[0]-rr/2, coords[1]-rr/2, rr, rr, 0,  360, Arc2D.PIE));
					g2.setColor(Color.BLACK);
					g2.drawString(Integer.toString(numPaths++), (float)(coords[0]+rr/4), (float)(coords[1]+rr/4));
					g2.setColor(colors.get(i));
					break;
				case PathIterator.SEG_LINETO:
					rr = 0.4*r;
					g2.setColor(darker(g2.getColor()));
					g2.fill(new Arc2D.Double(coords[0]-rr/2, coords[1]-rr/2, rr, rr, 0,  360, Arc2D.PIE));
					break;
				}
				pi.next();
			}
		}
	}

	private static final float FACTOR = 0.95f;
	static public Color darker(Color c) {
		return new Color(Math.max((int)(c.getRed()  *FACTOR), 0),
				Math.max((int)(c.getGreen()*FACTOR), 0),
				Math.max((int)(c.getBlue() *FACTOR), 0),
				c.getAlpha());
	}

	private Point2D arcCenter(Point2D p1, Point2D p2, double radius, boolean cw) {
		// returns arc center based on start and end points, radius and arc direction (cw)
		// Radius can be negative (for arcs over 180 degrees)
		double  angle    = 0;
		double  addAngle = 0;
		double  l1       = 0;
		double  l2       = 0;
		double  delta    = 0;
		Point2D tp1;
		Point2D tp2;

		// Sort points depending of direction
		if(cw) {
			tp1 = new Point2D.Double(p2.getX(), p2.getY());
			tp2 = new Point2D.Double(p1.getX(), p1.getY());
		} else {
			tp1 = new Point2D.Double(p1.getX(), p1.getY());
			tp2 = new Point2D.Double(p2.getX(), p2.getY());
		}

		// find angle arc covers
		angle = calcAngle(tp1, tp2);

		l1    = pointDistance(tp1, tp2) / 2;
		delta = l1 - Math.abs(radius);

		if (Math.abs(radius) < l1 && delta > EPSILON)
			throw new IllegalArgumentException("wrong radius");

		l2 = Math.sqrt(Math.abs(Math.pow(radius,2) - Math.pow(l1,2)));

		addAngle = l1 == 0 ? Math.PI / 2 : Math.atan(l2 / l1);

		// Add or subtract from angle (depending of radius sign)
		if (radius < 0)	angle -= addAngle;
		else			angle += addAngle;

		// calculate center (from T1)
		return new Point2D.Double((tp1.getX() + Math.abs(radius) * Math.cos(angle)), (tp1.getY() + Math.abs(radius) * Math.sin(angle)));
	}

	private double calcAngle(Point2D p1, Point2D p2) {
		// returns angle of line between 2 points and X axis (according to quadrants)
		double result = 0;

		if (p1 == p2) // same points
			return 0;
		else if (p1.getX() == p2.getX()) { // 90 or 270 
			result = Math.PI / 2;
			if (p1.getY() > p2.getY()) result += Math.PI;
		} else if (p1.getY() == p2.getY()) {  // 0 or 180
			result = 0;
			if (p1.getX() > p2.getX()) result += Math.PI;
		} else {
			result = Math.atan(Math.abs((p2.getY() - p1.getY()) / (p2.getX() - p1.getX()))); // 1. quadrant
			if (p1.getX() > p2.getX() && p1.getY() < p2.getY()) // 2. quadrant
				result = Math.PI - result;
			else if (p1.getX() > p2.getX() && p1.getY() > p2.getY()) // 3. quadrant
				result += Math.PI;
			else if (p1.getX() < p2.getX() && p1.getY() > p2.getY()) // 4. quadrant
				result = 2 * Math.PI - result;
		}
		return result;
	}

	private double pointDistance(Point2D p1, Point2D p2) {
		return Math.sqrt(Math.pow((p2.getX() - p1.getX()), 2) + Math.pow((p2.getY() - p1.getY()), 2));
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		scale += e.getWheelRotation() * -0.1f;
		if(scale < 0.1f) scale = 0.1f;
		if(scale > 3)    scale = 3;
		repaint();
	}
}
