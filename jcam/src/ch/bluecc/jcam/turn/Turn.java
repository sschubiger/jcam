package ch.bluecc.jcam.turn;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.filechooser.FileFilter;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;

@SuppressWarnings("nls")
public class Turn implements Runnable {
	private DataModel db;

	private JFrame     frameUI;
	private Preview    previewUI;
	private OpPanel    operationsUI;
	private JSplitPane splitUI;
	private JTextArea  messageUI;

	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		UIManager.setLookAndFeel(new NimbusLookAndFeel());
		SwingUtilities.invokeLater(new Turn());
	}

	private Turn() throws FileNotFoundException, IOException {
		db = new DataModel();
	}

	@Override
	public void run() {
		setupFrame();
		setupToolbar();
		if(db.getOperationsList().length == 0)
			db.newOperations();
		showFrame();
	}


	private void setupToolbar() {
		JPanel toolbarUI = new JPanel();
		toolbarUI.setLayout(new FlowLayout());

		JPopupMenu addUI = new JPopupMenu();
		for(Operation op : db.getOperations()) {
			JMenuItem item = new JMenuItem(op.toString());
			item.addActionListener((e)->db.insertOperation(op.copy()));
			UI.configure(item);
			addUI.add(item);
		}

		JButton newUI = UI.configure(new JButton("New"));
		newUI.addActionListener(e->db.newOperations());
		toolbarUI.add(newUI);

		final JFileChooser fc =  new JFileChooser();
		fc.setFileFilter(new FileFilter() {
			@Override
			public String getDescription() {return "jCAM";}

			@Override
			public boolean accept(File f) {
				return f.isDirectory() || f.getName().endsWith(".jcam");
			}
		});
		JButton openUI = UI.configure(new JButton("Open..."));
		openUI.addActionListener(e->{
			if (fc.showOpenDialog(openUI) == JFileChooser.APPROVE_OPTION)
				db.load(fc.getSelectedFile());
		});
		toolbarUI.add(openUI);

		JButton saveUI = UI.configure(new JButton("Save as..."));
		saveUI.addActionListener(e->{
			if (fc.showSaveDialog(saveUI) == JFileChooser.APPROVE_OPTION)
				db.saveAs(fc.getSelectedFile());
		});
		toolbarUI.add(saveUI);

		JButton opUI  = UI.configure(new JButton("Add Operation"));
		toolbarUI.add(opUI);
		opUI.addActionListener(e->addUI.show(opUI, 0, 0));

		frameUI.add(toolbarUI, BorderLayout.NORTH);
	}

	private void setupFrame() {
		frameUI              = new JFrame("jCAM::Turn (c) 2017-2018 BlueCC GmbH");
		frameUI.setLayout(new BorderLayout());
		previewUI            = new Preview(db);
		operationsUI         = new OpPanel(db);
		frameUI.add(messageUI = new JTextArea(4,80), BorderLayout.SOUTH);
		frameUI.add(new JNumberPad(), BorderLayout.EAST);
		messageUI.setForeground(Color.RED);
		messageUI.setEditable(false);
		messageUI.setLineWrap(true);
		db.setUI(operationsUI, previewUI, messageUI);
		JScrollPane scrollUI = new JScrollPane(operationsUI);
		splitUI              = new JSplitPane(JSplitPane.VERTICAL_SPLIT, scrollUI, previewUI);
		frameUI.add(splitUI, BorderLayout.CENTER);
	}

	private void showFrame() {
		frameUI.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frameUI.setSize(1400, 800);
		frameUI.setExtendedState(JFrame.MAXIMIZED_BOTH);
		frameUI.setVisible(true);
		splitUI.setDividerLocation(0.8);
	}	
}
