package ch.bluecc.jcam.turn;

import ch.bluecc.jcam.turn.DataModel.Change;

public interface IDataModelChangedListener {
	void changed(Change kind); 
}
