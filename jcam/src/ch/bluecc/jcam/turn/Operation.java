package ch.bluecc.jcam.turn;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.GeneralPath;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import ch.bluecc.jcam.turn.DataModel.Change;

@SuppressWarnings("nls")
public abstract class Operation implements MouseListener, MouseMotionListener, IDataModelChangedListener {
	static final double RESOLUTION   = 0.01;
	private static final double RESOLUTION_1 = 100; // 1 / RESOLUTION

	protected DataModel                 db;
	private   Map<String, JSpinner>     spinners = new TreeMap<>();
	private   Map<String, JComboBox<?>> combos   = new TreeMap<>();
	private   Map<String, Object>       initialProps;
	private   JLabel                    warnUI;
	private   JCheckBox                 enableUI;
	private   JComboBox<JLabel>         toolUI;
	private   JLabel                    toolIdUI;

	public Operation(DataModel db) {
		this.db = db;
	}

	protected JSpinner floatUI(double min, double max) {
		JSpinner result = new JSpinner(new SpinnerNumberModel(min, min, max, RESOLUTION));
		UI.configure(result);
		((JSpinner.DefaultEditor)result.getEditor()).getTextField().setColumns(5);
		result.addChangeListener(e->db.change(Change.VALUE));
		return result;
	}

	protected JSpinner intUI(int min, int max) {
		JSpinner result = new JSpinner(new SpinnerNumberModel(min, min, max, 1));
		UI.configure(result);
		((JSpinner.DefaultEditor)result.getEditor()).getTextField().setColumns(3);
		result.addChangeListener(e->db.change(Change.VALUE));
		return result;
	}

	protected JSpinner createOffset(JPanel parent) {
		JSpinner result;
		parent.add(new JLabel("Offset"));
		parent.add(result = floatUI(-db.getDriver().maxLength(),db.getDriver().maxLength()));
		result.setValue(0);
		property("offset", result);
		return result;
	}

	protected JSpinner createPitch(JPanel parent) {
		JSpinner result;
		parent.add(new JLabel("Pitch"));
		parent.add(result = floatUI(0, 3));
		result.setValue(0.5);
		property("pitch", result);
		return result;
	}

	protected JSpinner createOD(JPanel parent) {
		return createOD("", parent);
	}

	protected JSpinner createID(JPanel parent) {
		return createID("", parent);
	}

	protected JSpinner createOD(String prefix, JPanel parent) {
		JSpinner result;
		parent.add(new JLabel(prefix+"OD"));
		parent.add(result = floatUI(0, db.getDriver().maxOD()));
		result.setValue(1);
		property(prefix.toLowerCase().trim()+"od", result);
		return result;
	}

	protected JSpinner createSurfaceSpeed(JPanel parent, double speed) {
		JSpinner result;
		parent.add(new JLabel("Surface speed [m/s]"));
		parent.add(result = floatUI(db.getDriver().getMinFeedrate_mm_min(), db.getDriver().getMaxFeedrate_mm_min()));
		result.setValue(speed);
		property("surfaceSpeed", result);
		return result;
	}

	protected JSpinner createFeedRate(JPanel parent, double feed) {
		JSpinner result;
		parent.add(new JLabel("Feedrate [mm/m]"));
		parent.add(result = floatUI(db.getDriver().getMinFeedrate_mm_min(), db.getDriver().getMaxFeedrate_mm_min()));
		result.setValue(feed);
		property("feedRate", result);
		return result;
	}

	protected JSpinner createCutDepth(JPanel parent, double depth) {
		JSpinner result;
		parent.add(new JLabel("Cut depth"));
		parent.add(result = floatUI(db.getDriver().getMinCut_mm(), db.getDriver().getMaxCut_mm()));
		result.setValue(depth);
		property("cutDepth", result);
		return result;
	}

	protected JSpinner createID(String prefix, JPanel parent) {
		JSpinner result;
		parent.add(new JLabel(prefix+"ID"));
		parent.add(result = floatUI(0, db.getDriver().maxOD()));
		result.setValue(1);
		property(prefix.toLowerCase().trim()+"id", result);
		return result;
	}

	protected JSpinner createLength(JPanel parent, double min) {
		return createLength("Length", parent, min);
	}
	
	protected JSpinner createLength(String label, JPanel parent, double min) {
		JSpinner result;
		parent.add(new JLabel(label));
		parent.add(result = floatUI(min,db.getDriver().maxLength()));
		property("length", result);
		return result;
	}

	protected JSpinner createN(JPanel parent) {
		JSpinner result;
		parent.add(new JLabel("N"));
		parent.add(result = intUI(1, 25));
		property("n", result);
		return result;
	}

	protected double val(JSpinner ui) {
		int val = (int)(((Number)ui.getModel().getValue()).doubleValue() * RESOLUTION_1);
		return val / RESOLUTION_1;
	}

	@SuppressWarnings("unchecked")
	protected <T extends Operation> T op(JPanel op) {
		return (T) op.getClientProperty(Operation.class);
	}

	public JPanel createComponent() {
		JPanel result = new JPanel();
		result.setBorder(new TitledBorder(new EtchedBorder(), toString()));
		result.setLayout(new FlowLayout(FlowLayout.LEFT, 3, 0));
		result.putClientProperty(Operation.class, this);
		createChildren(result);
		result.add(warnUI = new JLabel(""));
		warnUI.setForeground(Color.RED);
		warnUI.setPreferredSize(new Dimension(200,18));
		warnUI.setMinimumSize(new Dimension(200,18));
		Dimension size = result.getPreferredSize();
		size.width = Integer.MAX_VALUE;
		result.setMaximumSize(size);
		return result;
	}

	Color getCutColor() {
		return skip() ? Color.GRAY : Color.DARK_GRAY;
	}

	protected void cut(Graphics2D g2, double mod, double lid, double rid, double offset, double length) {
		GeneralPath top = new GeneralPath();
		top.moveTo(offset,         -mod/2);
		top.lineTo(offset+length,  -mod/2);
		top.lineTo(offset+length, (-mod/2)+((mod-lid)/2));
		top.lineTo(offset,        (-mod/2)+((mod-rid)/2));
		top.closePath();

		GeneralPath bottom = new GeneralPath();
		bottom.moveTo(offset,         +mod/2);
		bottom.lineTo(offset+length,  +mod/2);
		bottom.lineTo(offset+length, (+mod/2)-((mod-lid)/2));
		bottom.lineTo(offset,        (+mod/2)-((mod-rid)/2));
		bottom.closePath();

		g2.setColor(getCutColor());
		g2.fill(top);
		g2.fill(bottom);
	}

	protected void bore(Graphics2D g2, double mod, double lid, double rid, double offset, double length) {
		GeneralPath top = new GeneralPath();
		top.moveTo(offset,         0);
		top.lineTo(offset+length,  0);
		top.lineTo(offset+length,  (lid/2));
		top.lineTo(offset,         (rid/2));
		top.closePath();

		GeneralPath bottom = new GeneralPath();
		bottom.moveTo(offset,         0);
		bottom.lineTo(offset+length,  0);
		bottom.lineTo(offset+length, -(lid/2));
		bottom.lineTo(offset,        -(rid/2));
		bottom.closePath();

		g2.setColor(getCutColor());
		g2.fill(top);
		g2.fill(bottom);
	}

	protected void trepan(Graphics2D g2, double mod, double od, double id, double offset, double length) {
		GeneralPath top = new GeneralPath();
		top.moveTo(offset,         (id/2));
		top.lineTo(offset+length,  (id/2));
		top.lineTo(offset+length,  (od/2));
		top.lineTo(offset,         (od/2));
		top.closePath();

		GeneralPath bottom = new GeneralPath();
		bottom.moveTo(offset,        -(id/2));
		bottom.lineTo(offset+length, -(id/2));
		bottom.lineTo(offset+length, -(od/2));
		bottom.lineTo(offset,        -(od/2));
		bottom.closePath();

		g2.setColor(getCutColor());
		g2.fill(top);
		g2.fill(bottom);
	}

	protected void thread(Graphics2D g2, double mod, double od, double id, double pitch, double offset, double length) {
		GeneralPath top = new GeneralPath();
		GeneralPath bottom = new GeneralPath();
		top.moveTo(offset, -mod/2);
		bottom.moveTo(offset,         +mod/2);

		for(double x = 0; x < length; x += pitch) {
			top.lineTo(offset+x,            (-mod/2)+((mod-od)/2));
			top.lineTo(offset+x+pitch/2,    (-mod/2)+((mod-id)/2));		
			top.lineTo(offset+x+pitch,      (-mod/2)+((mod-od)/2));

			bottom.lineTo(offset+x,         (+mod/2)-((mod-od)/2));
			bottom.lineTo(offset+x+pitch/2, (+mod/2)-((mod-id)/2));		
			bottom.lineTo(offset+x+pitch,   (+mod/2)-((mod-od)/2));
		}

		top.lineTo(offset+length,  -mod/2);
		bottom.lineTo(offset+length,  +mod/2);

		top.closePath();
		bottom.closePath();

		g2.setColor(getCutColor());
		g2.fill(top);
		g2.fill(bottom);
	}

	protected void createActions(JPanel parent) {
		parent.add(enableUI = new JCheckBox());
		enableUI.setSelected(true);
		enableUI.addActionListener(e->db.change(Change.VALUE));
		Operation op = op(parent);

		JLabel move = new JLabel(icon("move.png"));
		UI.configure(move);
		move.addMouseListener(this);
		move.addMouseMotionListener(this);
		parent.add(move);

		JButton delete = new JButton(icon("trash.png"));
		delete.addActionListener(e->db.deleteOperation(op));
		parent.add(delete);
		UI.configure(delete);
		JButton duplicate = new JButton(icon("duplicate.png"));
		duplicate.addActionListener(e->db.duplicateOperation(op));
		parent.add(duplicate);
		UI.configure(duplicate);
	}

	@Override
	public void mouseClicked(MouseEvent e) {}
	@Override
	public void mouseEntered(MouseEvent e) {}
	@Override
	public void mouseExited(MouseEvent e) {}
	@Override
	public void mouseMoved(MouseEvent e) {}
	@Override
	public void mousePressed(MouseEvent e) {
		JComponent  src   = (JComponent) e.getSource();
		JPanel      op    = (JPanel)src.getParent();
		OpPanel     panel = (OpPanel)op.getParent();
		panel.startDrag(SwingUtilities.convertMouseEvent(src, e, panel), op);
	}
	@Override
	public void mouseDragged(MouseEvent e) {
		JComponent  src   = (JComponent) e.getSource();
		JPanel      op    = (JPanel)src.getParent();
		OpPanel     panel = (OpPanel)op.getParent();
		panel.dragging(SwingUtilities.convertMouseEvent(src, e, panel));
	};
	@Override
	public void mouseReleased(MouseEvent e) {
		JComponent  src   = (JComponent) e.getSource();
		JPanel      op    = (JPanel)src.getParent();
		OpPanel     panel = (OpPanel)op.getParent();
		panel.stopDrag(SwingUtilities.convertMouseEvent(src, e, panel));
	};

	private Icon icon(String name) {
		return new ImageIcon(getClass().getResource("icons/"+name));
	}

	JComboBox<?> createTools(JPanel parent) {
		toolIdUI = new JLabel("0");
		toolUI = UI.createTools(db.getTools(), parent, null);
		parent.add(toolIdUI);
		property("tool", toolUI);
		toolUI.addActionListener(e->{
			if(toolUI.getItemCount() > 0) {
				db.change(Change.VALUE);	
				toolIdUI.setText(Integer.toString(getTool().getId()));
			}
		});
		toolIdUI.setText(Integer.toString(getTool().getId()));
		db.addListener(this);
		return toolUI;
	}

	@Override
	public void changed(Change kind) {
		if(kind == Change.TOOLS) {
			int selection = toolUI.getSelectedIndex();
			toolUI.removeAllItems();
			for(JLabel item : UI.createToolItems(db.getTools()))
				toolUI.addItem(item);
			toolUI.setSelectedIndex(Math.min(selection, toolUI.getItemCount()-1));
		}
	}

	protected abstract void createChildren(JPanel parent);

	public abstract void draw(Graphics2D g2);

	protected void property(String key, JSpinner ui) {
		spinners.put(key, ui);
		if(initialProps != null) {
			Object value = initialProps.get(key);
			if(value instanceof Number)
				ui.setValue(value);
		}
	}

	protected void property(String key, JComboBox<?> ui) {
		combos.put(key, ui);
		if(initialProps != null) {
			Object value = initialProps.get(key);
			if(value instanceof String) {
				for(int i = 0; i < ui.getItemCount(); i++) {
					if(UI.matches((String)value, ui, i)) {
						ui.setSelectedIndex(i);
						return;
					}
				}
			}
		}
	}

	public void setProperties(Map<String, Object> properties) {
		this.initialProps = properties;
	}

	public Operation copy() {
		try {
			Operation result = getClass().getConstructor(DataModel.class).newInstance(db);
			Map<String, Object> p = new TreeMap<>();
			for(String key : spinners.keySet())
				p.put(key, val(spinners.get(key)));
			for(String key : combos.keySet())
				p.put(key, combos.get(key).getSelectedItem().toString());
			result.setProperties(p);
			return result;
		} catch(Throwable t) {
			return null;
		}
	}

	public void write(FileWriter out) throws IOException {
		out.write(getClass().getName()+":");
		for(String key : spinners.keySet())
			out.write("D"+key+"="+val(spinners.get(key))+",");
		for(String key : combos.keySet())
			out.write("S"+key+"="+UI.toString(combos.get(key).getSelectedItem())+",");
		out.write('\n');
	}

	public void warning(String warn) {
		this.warnUI.setText(warn == null ? "" : warn);
	}

	public abstract double getTotalLength();
	public abstract double getLength();

	public Tool getTool() {
		return db.getTools()[toolUI.getSelectedIndex()];
	}

	public int getToolId() {
		return toolUI != null ? getTool().getId() : -1;
	}	

	public boolean skip() {
		return enableUI != null && !(enableUI.isSelected());
	}

	public void dispose() {
		db.removeListener(this);
	}
}
