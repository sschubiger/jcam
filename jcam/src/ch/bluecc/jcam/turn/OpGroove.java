package ch.bluecc.jcam.turn;

import java.awt.Graphics2D;

import javax.swing.JPanel;
import javax.swing.JSpinner;

@SuppressWarnings("nls")
public class OpGroove extends Operation implements IOffset {
	private JSpinner        nUI;
	private JSpinner        odUI;
	private JSpinner        offsetUI;

	public OpGroove(DataModel db) {
		super(db);
	}

	@Override
	protected void createChildren(JPanel parent) {
		createActions(parent);
		createTools(parent);
		nUI      = createN(parent);
		odUI     = createOD(parent);
		offsetUI = createOffset(parent);
	}

	@Override
	public void draw(Graphics2D g2) {
		double     toolW = getTool().getWidth();  
		OpMaterial mat   = db.getMaterial();
		double off = db.getOffset(this);
		for(int i = 0; i < val(nUI); i++, off += toolW + val(offsetUI))
			cut(g2, mat.getOD(), val(odUI), val(odUI), off + val(offsetUI), toolW);  
	}

	@Override
	public String toString() {
		return "Grooving";
	}

	@Override
	public double getLength() {
		double toolW = getTool().getWidth();  
		return (toolW + val(offsetUI));
	}
	
	@Override
	public double getTotalLength() {
		return getLength() * val(nUI);
	}

	public double getOD() {
		return val(odUI);
	}

	@Override
	public double getOffset() {
		return val(offsetUI);
	}
	
	public int getN() {
		return (int) val(nUI);
	}
}
