package ch.bluecc.jcam.turn;

import java.awt.Graphics2D;

import javax.swing.JPanel;
import javax.swing.JSpinner;

@SuppressWarnings("nls")
public class OpThreadExt extends Operation implements ICutLinear  {
	private JSpinner odUI;
	private JSpinner idUI;
	private JSpinner offsetUI;
	private JSpinner lenghtUI;
	private JSpinner pitchUI;
	
	public OpThreadExt(DataModel db) {
		super(db);
	}

	@Override
	protected void createChildren(JPanel parent) {
		createActions(parent);
		createTools(parent);
		odUI     = createOD(parent);
		idUI     = createID(parent);
		pitchUI  = createPitch(parent);
		offsetUI = createOffset(parent);
		lenghtUI = createLength(parent, 0);
	}

	@Override
	public void draw(Graphics2D g2) {
		double     toolW = getTool().getWidth();  
		warning(toolW > val(lenghtUI) ? "Tool width ("+toolW+") > length" : null);	
		OpMaterial mat   = db.getMaterial();
		double off = db.getOffset(this);
		thread(g2, mat.getOD(), val(odUI), val(idUI), val(pitchUI), off + val(offsetUI), val(lenghtUI));  
	}

	@Override
	public String toString() {
		return "External Threading";
	}

	@Override
	public double getLength() {
		return getCutLength() + val(offsetUI);
	}

	public double getCutLength() {
		return val(lenghtUI);
	}
	
	@Override
	public double getTotalLength() {
		return getLength();
	}

	public double getOD() {
		return val(odUI);
	}

	public double getID() {
		return val(idUI);
	}

	@Override
	public double getOffset() {
		return val(offsetUI);
	}

	@Override
	public int getN() {
		return 1;
	}

	@Override
	public double getLeftD() {
		return getOD();
	}

	@Override
	public double getRightD() {
		return getOD();
	}

	public double getPitch() {
		return val(pitchUI);
	}
}
