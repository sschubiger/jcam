package ch.bluecc.jcam.turn;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import ch.bluecc.jcam.Material;
import ch.bluecc.jcam.driver.DenfordOrac;
import ch.bluecc.jcam.driver.Driver;
import ch.bluecc.jcam.gcode.Program;
import ch.bluecc.jcam.turn.OpMaterial.Speed;

@SuppressWarnings("nls")
public class DataModel {	
	private static final File STATE       = new File("state.jcam");
	public  static final int  N_DEF_ITEMS = 2;

	enum Change {TOOLS, VALUE, STRUCTURE}
	
	private Driver[]        drivers    = {
			new DenfordOrac(),
	};
	private Operation[]     operations = {
			new OpFace(this),
			new OpCutConst(this),
			new OpCutLinear(this),
			new OpChamferLeft(this),
			new OpChamferRight(this),
			new OpCutCircleLeft(this),
			new OpCutCircleRight(this),
			new OpGroove(this),
			new OpBore(this),
			new OpTrepan(this),
			new OpThreadExt(this),
			new OpPart(this),
	};

	private List<Material>                  materials  = new ArrayList<>();
	private List<Tool>                      toolLib    = new ArrayList<>();
	private OpPanel                         operationsUI;
	private JTextArea                       messageUI;
	private Preview                         previewUI;
	private List<IDataModelChangedListener> listeners = new ArrayList<>();

	public DataModel() throws FileNotFoundException, IOException {
		Properties mp = Material.getProperties();
		for(int i = 0; mp.containsKey(Integer.toString(i)); i++)
			materials.add(new Material(mp, i));

		toolLib.add(new Tool());
		Properties tp = Tool.getProperties();
		for(int i = 0; tp.containsKey(Integer.toString(i)); i++)
			toolLib.add(new Tool(tp, i));

		
		Runtime.getRuntime().addShutdownHook(new Thread(()->{
			saveAs(STATE);
		}));
	}

	public Material[] getMaterials() {
		return materials.toArray(new Material[materials.size()]);
	}

	public Operation[] getOperations() {
		return operations;
	}

	public Tool[] getToolLib() {
		return toolLib.toArray(new Tool[toolLib.size()]);
	}

	public Tool[] getTools() {
		List<Tool> tools = new ArrayList<>();
		Properties tp = ToolSelector.getProperties();
		for(int i = 0; i <= getDriver().numToolSlots(); i++) {
			if(tp.containsKey(Integer.toString(i)))
				tools.add(new Tool(tp, i));
		}
		if(tools.isEmpty())	tools.add(new Tool());
		return tools.toArray(new Tool[tools.size()]);
	}

	public Driver[] getDrivers() {
		return drivers;
	}

	public void setUI(OpPanel macrosUI, Preview previewUI, JTextArea messageUI) {
		this.operationsUI = macrosUI;
		this.previewUI    = previewUI;
		this.messageUI    = messageUI;
		load(STATE);
	}

	public void insertOperation(Operation macro) {
		insertOperation(macro, -1);
	}

	public void insertOperation(Operation op, int idx) {
		JComponent comp = op.createComponent();
		operationsUI.add(comp, idx);
		structureChanged();
	}

	public void deleteOperation(Operation op) {
		for(JPanel cmp : operationsUI.getOpComponents())
			if(cmp.getClientProperty(Operation.class) == op) {
				operationsUI.remove(cmp);
				op.dispose();
			}
		structureChanged();
	}

	public void duplicateOperation(Operation macro) {
		int idx          = -1;
		int i            = 0;
		for(JPanel cmp : operationsUI.getOpComponents()) {
			if(cmp.getClientProperty(Operation.class) == macro) {
				idx = i;
				break;
			}
			i++;
		}
		if(idx >= 0)
			insertOperation(macro.copy(), idx+1);
	}

	public void change(Change kind) {
		matCache    = null;
		driverCache = null;
		if(previewUI != null) {
			previewUI.setProgram(null);
			previewUI.repaint();
		}
		for(IDataModelChangedListener l : listeners)
			l.changed(kind);
	}
	
	public void structureChanged() {
		opCache = null;
		if(operationsUI != null && operationsUI.getParent() != null) {
			operationsUI.getParent().validate();
			operationsUI.getParent().repaint();
		}
		change(Change.STRUCTURE);
	}

	private Operation[] opCache;
	public Operation[] getOperationsList() {
		if(operationsUI == null) return new Operation[0];
		if(opCache == null) {
			JPanel[] cmps = operationsUI.getOpComponents();
			opCache = new Operation[cmps.length];
			for(int i = 0; i < opCache.length; i++)
				opCache[i] = (Operation)cmps[i].getClientProperty(Operation.class);
		}
		return opCache;
	}

	private OpMaterial matCache;
	public OpMaterial getMaterial() {
		if(matCache == null) {
			for(Operation op : getOperationsList()) {
				if(op instanceof OpMaterial) {
					matCache = (OpMaterial) op;
					break;
				}
			}
		}
		return matCache;
	}

	private OpDriver driverCache;
	public OpDriver getDriver() {
		if(driverCache == null) {
			for(Operation op : getOperationsList()) {
				if(op instanceof OpDriver) {
					driverCache = (OpDriver) op;
					break;
				}
			}
		}
		return driverCache;
	}

	public double getOffset(Operation op) {
		double result = 0;
		for(Operation o : getOperationsList()) {
			if(o == op) break;
			if(o instanceof OpFace || o instanceof OpMaterial)
				result = 0;
			else
				result += o.getTotalLength();
		}
		return result;
	}

	public static String getString(Properties props, int i, String key) {
		String properties = props.getProperty(Integer.toString(i));
		if(properties != null) {
			for(String prop : properties.split("[;]")) {
				String[] keyVal = prop.trim().split("[=]");
				if(key.equals(keyVal[0])) return keyVal[1];
			}
		}
		return null;
	}

	public static double getDouble(Properties props, int i, String key, double def) {
		try {
			return Double.parseDouble(getString(props, i, key));
		} catch(Throwable t) {
			return def;
		}
	}

	public static int getInt(Properties props, int i, String key, int def) {
		try {
			return Integer.parseInt(getString(props, i, key));
		} catch(Throwable t) {
			return def;
		}
	}

	public static boolean getBoolean(Properties props, int i, String key, boolean def) {
		try {
			return Boolean.parseBoolean(getString(props, i, key));
		} catch(Throwable t) {
			return def;
		}
	}

	public static double[] getDoubles(Properties props, int i, String key) {
		try {
			String[] doubles = getString(props, i, key).split("[,]");
			double[] result  = new double[doubles.length];
			for(i = 0; i < result.length; i++)
				result[i] = Double.parseDouble(doubles[i]);
			return result;
		} catch(Throwable t) {
			return null;
		}
	}

	public static double[] getDoubleRange(Properties props, int i, String key) {
		String[] range = getString(props, i, key).split("[-]");
		return new double[] {Double.parseDouble(range[0]), Double.parseDouble(range[1])};
	}

	public void newOperations() {
		opCache = null;
		operationsUI.removeAll();
		insertOperation(new OpDriver(this));
		insertOperation(new OpMaterial(this));
	}

	public void load(File file) {
		operationsUI.removeAll();
		opCache = null;
		try(BufferedReader in = new BufferedReader(new FileReader(file))) {
			for(;;) {
				String line = in.readLine();
				if(line == null) break;
				try {
					int idx = line.indexOf(':');
					Operation m = (Operation) Class.forName(line.substring(0, idx)).getConstructor(DataModel.class).newInstance(this);
					Map<String, Object> p = new TreeMap<>();
					for(String prop : line.substring(idx+1).split("[,]")) {
						String[] kv = prop.split("[=]");
						if(kv[0].charAt(0) == 'D')
							p.put(kv[0].substring(1), new Double(kv[1]));
						if(kv[0].charAt(0) == 'S')
							p.put(kv[0].substring(1), kv[1].toString());
					}
					m.setProperties(p);
					insertOperation(m);
				} catch(Throwable t) {
					error(t);
				}
			}
		} catch (Throwable t) {
			error(t);
		}
	}

	public void saveAs(File file) {
		if(!(file.getName().endsWith(".jcam")))
			file = new File(file.getParentFile(), file.getName() + ".jcam");
		try(FileWriter out = new FileWriter(file)) {
			for(Operation op : getOperationsList())
				op.write(out);
		} catch (Throwable t) {
			error(t);
		}
	}

	public void error(Throwable t) {
		StringWriter sw = new StringWriter();
		t.printStackTrace(new PrintWriter(sw));
		messageUI.setText(sw.toString().replace('\n', '|'));
	}

	public double speedRPM(double diameter_mm, Speed speed) {
		double surfaceSpeed_mm_s = getMaterial().surfaceSpeed_m_s(speed) * 1000;
		double rps               = surfaceSpeed_mm_s / (diameter_mm * Math.PI);
		double rpm               = rps * 60;
		OpDriver driver          = getDriver();
		return Math.min(Math.max(driver.minRPM(), rpm), driver.maxRPM());
	}

	public void setProgram(Program program) {
		if(previewUI != null)
			previewUI.setProgram(program);
	}
	
	public void addListener(IDataModelChangedListener l) {
		listeners.add(l);
	}
	
	public void removeListener(IDataModelChangedListener l) {
		listeners.remove(l);
	}

	public double cutDepth_mm() {
		return getMaterial().cutDepth_mm();
	}

	public double feedRate_mm_s() {
		return getMaterial().feedRate_mm_s();
	}

}
