package ch.bluecc.jcam.turn;

import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;

import ch.bluecc.jcam.Material;
import ch.bluecc.jcam.turn.DataModel.Change;

@SuppressWarnings("nls")
public class OpMaterial extends Operation {
	public enum Speed {STRAIGHT_MIN,STRAIGHT_MAX,STRAIGHT_AVG};

	private JComboBox<?>        materialUI;
	private JSpinner            odUI;
	private JSpinner            lenghtUI;
	private JSpinner            surfaceSpeedUI;
	private JSpinner            feedRateUI;
	private JSpinner            cutDepthUI;
	double[]                    speedRange;
	
	public OpMaterial(DataModel db) {
		super(db);
	}

	@Override
	protected void createChildren(JPanel parent) {
		Material[] materials = db.getMaterials();
		JLabel[]   items     = new JLabel[materials.length];
		for(int i = 0; i < items.length; i++) {
			BufferedImage texture = materials[i].getTexture();
			items[i] = new JLabel(materials[i].toString(), new ImageIcon(texture.getSubimage(0, 0, texture.getWidth(), 32)), JLabel.LEFT);
		}
		materialUI = new JComboBox<>(items);
		materialUI.setEditable(false);
		materialUI.setRenderer(new UI.JLabelRenderer(JLabel.CENTER, JLabel.CENTER));
		parent.add(materialUI);
		property("material", materialUI);
		odUI           = createOD(parent);
		lenghtUI       = createLength(parent, 1);
		surfaceSpeedUI = createSurfaceSpeed(parent, std_surfaceSpeed_m_s(Speed.STRAIGHT_AVG));
		feedRateUI     = createFeedRate(parent, std_feedrate_mm_s());
		cutDepthUI     = createCutDepth(parent, std_cutDepth_mm());
		materialUI.addActionListener(e->{
			surfaceSpeedUI.setValue(std_surfaceSpeed_m_s(Speed.STRAIGHT_AVG));
			feedRateUI.setValue(std_feedrate_mm_s());
			cutDepthUI.setValue(std_cutDepth_mm());
			db.change(Change.VALUE);
		});
	}

	public double std_surfaceSpeed_m_s(Speed speed) {
		speedRange = getMaterial().getSpeedRange_m_s();
		return speedRange[0] + (speedRange[1]-speedRange[0])/2;
	}

	private double std_feedrate_mm_s() {
		return Math.min(Math.max(surfaceSpeed_m_s(Speed.STRAIGHT_MAX) * 1.5, db.getDriver().getMinFeedrate_mm_min()), db.getDriver().getMaxFeedrate_mm_min());
	}

	private double std_cutDepth_mm() {
		return Math.min(Math.max(surfaceSpeed_m_s(Speed.STRAIGHT_MIN) / 100, db.getDriver().getMinCut_mm()), db.getDriver().getMaxCut_mm());
	}

	@Override
	public String toString() {
		return "Material";
	}

	@Override
	public void draw(Graphics2D g2) {
		g2.setPaint(getMaterial().getPaint());
		g2.fill(new Rectangle2D.Double(0, -val(odUI)/2, val(lenghtUI), val(odUI)));
	}

	public double getOD() {
		return val(odUI);
	}

	@Override
	public double getTotalLength() {
		return getLength();
	}

	@Override
	public double getLength() {
		return val(lenghtUI);
	}

	private Material getMaterial() {
		return db.getMaterials()[materialUI.getSelectedIndex()];
	}


	public double surfaceSpeed_m_s(Speed speed) {
		return val(surfaceSpeedUI);
	}
	
	public double cutDepth_mm() {
		return val(cutDepthUI);
	}

	public double feedRate_mm_s() {
		return val(feedRateUI);
	}
}
