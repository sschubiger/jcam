package ch.bluecc.jcam.turn;

import java.awt.Graphics2D;

import javax.swing.JPanel;
import javax.swing.JSpinner;

@SuppressWarnings("nls")
public class OpCutConst extends Operation implements ICutLinear  {
	private JSpinner        nUI;
	private JSpinner        odUI;
	private JSpinner        offsetUI;
	private JSpinner        lenghtUI;

	public OpCutConst(DataModel db) {
		super(db);
	}

	@Override
	protected void createChildren(JPanel parent) {
		createActions(parent);
		createTools(parent);
		nUI      = createN(parent);
		odUI     = createOD(parent);
		offsetUI = createOffset(parent);
		lenghtUI = createLength(parent, 0);
	}

	@Override
	public void draw(Graphics2D g2) {
		double     toolW = getTool().getWidth();  
		warning(toolW > val(lenghtUI) ? "Tool width ("+toolW+") > length" : null);	
		OpMaterial mat   = db.getMaterial();
		double off = db.getOffset(this);
		for(int i = 0; i < val(nUI); i++, off += val(lenghtUI) + val(offsetUI))
			cut(g2, mat.getOD(), val(odUI), val(odUI), off + val(offsetUI), val(lenghtUI));  
	}

	@Override
	public String toString() {
		return "Cutting";
	}

	@Override
	public double getLength() {
		return val(lenghtUI) + val(offsetUI);
	}
	
	@Override
	public double getTotalLength() {
		return getLength() * val(nUI);
	}

	public double getOD() {
		return val(odUI);
	}

	@Override
	public double getOffset() {
		return val(offsetUI);
	}

	@Override
	public int getN() {
		return (int) val(nUI);
	}

	@Override
	public double getLeftD() {
		return getOD();
	}

	@Override
	public double getRightD() {
		return getOD();
	}
}
