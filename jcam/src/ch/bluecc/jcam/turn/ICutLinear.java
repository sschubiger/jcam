package ch.bluecc.jcam.turn;

public interface ICutLinear extends IOffset {
	int    getN();
	double getLeftD();
	double getRightD();
}
