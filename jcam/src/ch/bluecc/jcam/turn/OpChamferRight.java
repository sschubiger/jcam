package ch.bluecc.jcam.turn;

import java.awt.Graphics2D;

import javax.swing.JPanel;
import javax.swing.JSpinner;

@SuppressWarnings("nls")
public class OpChamferRight extends Operation implements ICutLinear {
	private JSpinner        odUI;
	private JSpinner        lenghtUI;
	
	public OpChamferRight(DataModel db) {
		super(db);
	}

	@Override
	protected void createChildren(JPanel parent) {
		createActions(parent);
		createTools(parent);
		odUI     = createOD(parent);
		lenghtUI = createLength(parent, 0);
	}

	@Override
	public void draw(Graphics2D g2) {
		double     toolW = getTool().getWidth();  
		warning(toolW > val(lenghtUI) ? "Tool width ("+toolW+") > length" : null);	
		OpMaterial mat   = db.getMaterial();
		cut(g2, mat.getOD(), val(odUI), val(odUI)-getLength()*2, db.getOffset(this), getLength());  
	}

	@Override
	public String toString() {
		return "Right chamfering";
	}

	@Override
	public double getLength() {
		return val(lenghtUI);
	}
	
	@Override
	public double getTotalLength() {
		return getLength();
	}

	public double getOD() {
		return val(odUI);
	}

	@Override
	public int getN() {
		return 1;
	}

	@Override
	public double getOffset() {
		return 0;
	}

	@Override
	public double getLeftD() {
		return getOD();
	}

	@Override
	public double getRightD() {
		return getLeftD()-getLength()*2;
	}
}
