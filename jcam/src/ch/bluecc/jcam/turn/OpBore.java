package ch.bluecc.jcam.turn;

import java.awt.Graphics2D;

import javax.swing.JPanel;
import javax.swing.JSpinner;

@SuppressWarnings("nls")
public class OpBore extends Operation  implements ICutLinear  {
	private JSpinner        lidUI;
	private JSpinner        ridUI;
	private JSpinner        lenghtUI;

	public OpBore(DataModel db) {
		super(db);
	}

	@Override
	protected void createChildren(JPanel parent) {
		createActions(parent);
		createTools(parent);
		lidUI    = createID("Left ",  parent);
		ridUI    = createID("Right ", parent);
		lenghtUI = createLength(parent, 0);
	}

	@Override
	public void draw(Graphics2D g2) {
		double     toolW = getTool().getWidth();  
		warning(toolW > val(lenghtUI) ? "Tool width ("+toolW+") > length" : null);	
		OpMaterial mat   = db.getMaterial();
		double off = db.getOffset(this);
		bore(g2, mat.getOD(), val(lidUI), val(ridUI), off, val(lenghtUI));  
	}

	@Override
	public String toString() {
		return "Boring (linear)";
	}

	@Override
	public double getLength() {
		return val(lenghtUI);
	}

	@Override
	public double getTotalLength() {
		return 0;
	}

	@Override
	public double getLeftD() {
		return val(lidUI);
	}

	@Override
	public double getRightD() {
		return val(ridUI);
	}

	@Override
	public double getOffset() {
		return 0;
	}

	@Override
	public int getN() {
		return 1;
	}
}
