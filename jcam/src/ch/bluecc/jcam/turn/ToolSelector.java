package ch.bluecc.jcam.turn;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Properties;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;

import ch.bluecc.jcam.driver.Driver;
import ch.bluecc.jcam.turn.DataModel.Change;

public class ToolSelector extends JDialog {
	private static final long serialVersionUID = -803066650252369771L;

	private static final File TOOLPOST = new File("toolpost.properties"); //$NON-NLS-1$
	private static Properties props;
	private enum Conf {XOFF,ZOFF}

	@SuppressWarnings("nls")
	public ToolSelector(JComponent owner, DataModel db, Driver driver) {
		super(SwingUtilities.windowForComponent(owner), "Tools");
		setModal(true);
		setLayout(new BorderLayout());
		JPanel panel  = new JPanel();
		add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridBagLayout());
		panel.add(new JLabel("Slot"),     gbc(0, 0, 0));
		panel.add(new JLabel("Tool"),     gbc(1, 0, 0));
		panel.add(new JLabel("X-Offset"), gbc(2, 0, 0));
		panel.add(new JLabel("Z-Offset"), gbc(3, 0, 0));
		getProperties();
		for(int slot = 1; slot <= driver.numToolSlots(); slot++) {
			JLabel label = new JLabel(Integer.toString(slot));
			UI.configure(label);
			panel.add(label, gbc(0, slot, 2));
			JComboBox<?> toolChooser = UI.createTools(db.getToolLib(), panel, gbc(1, slot, 5));
			if(props.containsKey(Integer.toString(slot))) {
				Tool tmp = new Tool(props, slot);
				int idx = 0;
				for(Tool t : db.getToolLib()) {
					if(t.equals(tmp)) {
						toolChooser.setSelectedIndex(idx);
						t.setXoff(tmp.getXoff());
						t.setZoff(tmp.getZoff());
					}
					idx++;
				}
			}
			int fslot = slot;
			toolChooser.addActionListener(e->storeProps(db, fslot, toolChooser));
			panel.add(floatUI(db, slot, toolChooser, -driver.maxLength_mm(), driver.maxLength_mm(), Conf.XOFF), gbc(2, slot, 2));
			panel.add(floatUI(db, slot, toolChooser, -driver.maxOD_mm(),     driver.maxOD_mm(),     Conf.ZOFF), gbc(3, slot, 2));
		}
		add(new JNumberPad(), BorderLayout.EAST);
		pack();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);	
	}

	protected JSpinner floatUI(DataModel db, int slot, JComboBox<?> toolChooser, double min, double max, Conf conf) {
		Tool tool = db.getToolLib()[toolChooser.getSelectedIndex()];
		double value = 0;
		switch (conf) {
		case XOFF: value = tool.getXoff(); break;
		case ZOFF: value = tool.getZoff(); break;
		}
		JSpinner result = new JSpinner(new SpinnerNumberModel(value, min, max, Operation.RESOLUTION));
		UI.configure(result);
		((JSpinner.DefaultEditor)result.getEditor()).getTextField().setColumns(5);
		result.addChangeListener(e->{
			switch(conf) {
			case XOFF: tool.setXoff(((Number)result.getValue()).doubleValue()); break;
			case ZOFF: tool.setZoff(((Number)result.getValue()).doubleValue()); break;
			}
			storeProps(db, slot, toolChooser);
		}
				);
		return result;
	}

	@SuppressWarnings("nls")
	private void storeProps(DataModel db, int slot, JComboBox<?> toolChooser) {
		try {
			String sSlot = Integer.toString(slot);
			Tool tool = db.getToolLib()[toolChooser.getSelectedIndex()];
			if(toolChooser.getSelectedIndex() == 0) {
				tool.setId(-1);
				props.remove(sSlot);
			} else {
				tool.setId(slot);
				props.put(sSlot, tool.toProps());
			}
			db.change(Change.TOOLS);
			props.store(new FileWriter(TOOLPOST), "Toolpost");
		} catch (Throwable t) {
			db.error(t);
		}
	}

	private GridBagConstraints gbc(int x, int y, double wx) {
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx   = x; 
		gbc.gridy   = y;
		gbc.weightx = wx;
		return gbc;
	}

	public static Properties getProperties() {
		if(props == null) {
			props = new Properties();
			try {
				props.load(new FileReader(TOOLPOST));
			} catch (Throwable e) {}
		}
		return props;
	}
}
