package ch.bluecc.jcam.turn;

import java.awt.Graphics2D;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ch.bluecc.jcam.driver.Driver;
import ch.bluecc.jcam.driver.Driver.Rate;
import ch.bluecc.jcam.gcode.Program;

@SuppressWarnings("nls")
public class OpDriver extends Operation {
	private JComboBox<String> portsUI;
	private JComboBox<Rate>   ratesUI;
	private JComboBox<Driver> driversUI;
	private JButton           sendUI;
	private JButton           simulateUI;
	private JLabel            gcodesUI;
	private JButton           toolsUI;

	public OpDriver(DataModel db) {
		super(db);
	}

	@Override
	protected void createChildren(JPanel parent) {
		parent.add(UI.configure(driversUI  = new JComboBox<>(db.getDrivers())));
		parent.add(UI.configure(portsUI    = new JComboBox<>()));
		parent.add(UI.configure(ratesUI    = new JComboBox<>()));
		parent.add(UI.configure(sendUI     = new JButton("Send...")));
		parent.add(UI.configure(simulateUI = new JButton("Simulate")));
		parent.add(UI.configure(toolsUI    = new JButton("Tools")));
		parent.add(UI.configure(gcodesUI   = new JLabel("")));

		driversUI.addActionListener(e->this.updateDriver());
		updateDriver();

		sendUI.addActionListener(e->{
			try {
				getDriver().send(portsUI.getSelectedItem().toString(), (Rate)ratesUI.getSelectedItem(), db, db.getOperationsList());
			} catch(Throwable t) {
				db.error(t);
			}});

		simulateUI.addActionListener(e->{
			gcodesUI.setText("");
			Program p = getDriver().toProgram(db, db.getOperationsList());
			gcodesUI.setText(p.getFlatCode().size() + " of " + db.getDriver().maxGCodes() + " GCodes used");
			db.setProgram(p);	
		});

		toolsUI.addActionListener(e->{
			new ToolSelector(toolsUI, db, getDriver()).setVisible(true);
		});

		property("driver", driversUI);
		property("port",   portsUI);
		property("rate",   ratesUI);
	}

	@Override
	public void draw(Graphics2D g2) {}

	@Override
	public double getTotalLength() {return 0;}

	@Override
	public double getLength() {return 0;}

	private void updateDriver() {
		Driver driver = getDriver();
		portsUI.removeAllItems();
		ratesUI.removeAllItems();
		for(String port : driver.getPorts())
			portsUI.addItem(port);
		for(Rate   rate : driver.getRates()) 
			ratesUI.addItem(rate);
		if(portsUI.getItemCount() > 0)
			portsUI.setSelectedIndex(0);
		if(ratesUI.getItemCount() > 0)
			ratesUI.setSelectedIndex(0);
	}

	private Driver getDriver() {
		return (Driver) driversUI.getSelectedItem();
	}

	@Override
	public String toString() {
		return "Driver";
	}

	public double maxOD() {
		return getDriver().maxOD_mm();
	}

	public double maxLength() {
		return getDriver().maxLength_mm();
	}

	public double minRPM() {
		return getDriver().minRPM();
	}

	public double maxRPM() {
		return getDriver().maxRPM();
	}

	public int maxGCodes() {
		return getDriver().maxGCodes();
	}

	public double getMaxFeedrate_mm_min() {
		return getDriver().getMaxFeedrate_mm_min();
	}

	public double getMinFeedrate_mm_min() {
		return getDriver().getMinFeedrate_mm_min();
	}

	public double getMinCut_mm() {
		return getDriver().resolution_mm();
	}

	public double getResolution_mm() {
		return getDriver().resolution_mm();
	}

	public double getMaxCut_mm() {
		return getDriver().getMaxCut_mm();
	}

	public double getSafeDist() {
		return getDriver().getSafeDist_mm();
	}

	public int numToolSlots() {
		return getDriver().numToolSlots();
	}
}
