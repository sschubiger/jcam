package ch.bluecc.jcam.turn;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import javax.swing.ImageIcon;

@SuppressWarnings("nls")
public class Tool implements Comparable<Tool> {
	public enum Cut {left, mid, right, both}
	
	private String    name;
	private double    width;
	private int       id;
	private Cut       cut;
	private ImageIcon image;
	private double    xoff;
	private double    zoff;
	
	public Tool() {
		name = "None";
		id   = -1;
		cut  =  Cut.both;
	}
	
	public Tool(Properties props, int i) {
		name     = DataModel.getString (props, i, "name");
		width    = DataModel.getDouble (props, i, "width", 0);
		cut      = Cut.valueOf(DataModel.getString(props, i, "cut"));
		id       = DataModel.getInt(props, i, "id", 0);
		xoff     = DataModel.getDouble (props, i, "xoff", 0);
		zoff     = DataModel.getDouble (props, i, "zoff", 0);
		try {
			image = new ImageIcon(getClass().getResource("toolimgs/" + name + ".png"));
		} catch(Throwable t) {}
	}

	public String toProps() {
		return 
				"name="+name+
				";width="+width+
				";id="+id+
				";cut="+cut+
				";xoff="+xoff+
				";zoff="+zoff;
	}

	@Override
	public String toString() {
		return name;
	}

	public double getWidth() {
		return width;
	}

	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public double getXoff() {
		return xoff;
	}
	
	public void setXoff(double xoff) {
		this.xoff = xoff;
	}
	
	public double getZoff() {
		return zoff;
	}
	
	public void setZoff(double zoff) {
		this.zoff = zoff;
	}
	
	public Cut getCut() {
		return cut;
	}

	public ImageIcon getImage() {
		return image;
	}
	
	public static Properties getProperties() throws FileNotFoundException, IOException {
		Properties result = new Properties();
		try(BufferedReader in = new BufferedReader(new InputStreamReader(Tool.class.getResourceAsStream("tool.properties")))) {
			for(int lineNo = 0; ;lineNo++) {
				String line = in.readLine();
				if(line == null) break;
				line = line.trim();
				if(line.length() == 0 || line.startsWith("#")) continue;
				result.put(Integer.toString(lineNo), line);
			}
		}
		return result;
	}

	@Override
	public int compareTo(Tool o) {
		return Integer.compare(getId(), o.getId());
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj instanceof Tool && ((Tool)obj).name.equals(name);
	}
	
	@Override
	public int hashCode() {
		return name.hashCode();
	}
}
