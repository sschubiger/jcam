package ch.bluecc.jcam.turn;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;

public class OpPanel extends Box {
	private static final long serialVersionUID = -8924429161626331127L;

	private Point      dragPoint;
	private JComponent opCmp;
	private DataModel  db;

	public OpPanel(DataModel db) {
		super(BoxLayout.Y_AXIS);
		this.db = db;
	}

	public void startDrag(MouseEvent e, JPanel op) {
		this.dragPoint = e.getPoint();
		this.opCmp        = op;
		repaint();
	}

	public void stopDrag(MouseEvent e) {
		if(opCmp != null)  {
			JComponent dst = findComponent(e.getPoint());
			if(dst != null) {
				int srcIdx = getIndex(opCmp);
				int dstIdx = getIndex(dst);
				if(dragPoint.y > dst.getY() + dst.getHeight() / 2)
					dstIdx++;
				if(dstIdx > srcIdx)
					dstIdx--;
				if(srcIdx != dstIdx) {
					remove(opCmp);
					add(opCmp, dstIdx);
					db.structureChanged();
				}
			}
			dragPoint = null;
			opCmp        = null;
			repaint();
		}
	}

	public void dragging(MouseEvent e) {
		this.dragPoint = e.getPoint();
		if(opCmp != null)
			repaint();
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(Color.BLUE);
		if(dragPoint != null) {
			Component cmp = findComponent(dragPoint);
			if(cmp != null) {
				int y = dragPoint.y < cmp.getY() + cmp.getHeight() / 2 ? cmp.getY() : cmp.getY() + cmp.getHeight(); 
				g.fillRect(0, y, getWidth(), 3);				
			}
		}
	}

	private JPanel findComponent(Point p) {
		for(JPanel cmp : getOpComponents())
			if(dragPoint.y >= cmp.getY() && dragPoint.y < cmp.getY() + cmp.getHeight() && getIndex(cmp) >= DataModel.N_DEF_ITEMS)
				return cmp;
		return null;
	}

	public int getIndex(Component cmp) {
		JPanel[] cmps = getOpComponents();
		for(int i = 0; i < cmps.length; i++)
			if(cmps[i] == cmp)
				return i;
		return -1;
	}

	public JPanel[] getOpComponents() {
		JPanel[] result = new JPanel[getComponentCount()];
		int i = 0;
		for(Component cmp : getComponents())
			if(cmp instanceof JPanel)
				result[i++] = (JPanel)cmp;
		return result;
	}	
}
