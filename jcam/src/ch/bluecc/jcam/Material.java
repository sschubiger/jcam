package ch.bluecc.jcam;

import java.awt.Color;
import java.awt.Paint;
import java.awt.TexturePaint;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.imageio.ImageIO;

import ch.bluecc.jcam.turn.DataModel;

@SuppressWarnings("nls")
public class Material {
	private String        name;
	private Paint         paint;
	private BufferedImage texture;
	private double[]      straightSpeed;

	public Material(Properties props, int i) {
		this.name           = DataModel.getString(props,      i, "name");
		this.straightSpeed  = DataModel.getDoubleRange(props, i, "straightSpeed");
		try {
			this.paint  = Color.decode(DataModel.getString(props, i, "color"));
		} catch(Throwable t) {
			this.paint = Color.WHITE;
		}
		try {
			this.texture = ImageIO.read(getClass().getResourceAsStream("imgs/" + name + ".jpg"));
			this.paint   = new TexturePaint(texture, new Rectangle2D.Double(0,0, texture.getWidth()/10.0, texture.getHeight()/10.0));
		} catch(Throwable t) {
		}
	}

	@Override
	public String toString() {
		return name;
	}

	public double[] getSpeedRange_m_s() {
		return straightSpeed;
	}

	public Paint getPaint() {
		return paint;
	}
	
	public static Properties getProperties() throws FileNotFoundException, IOException {
		Properties result = new Properties();
		try(InputStream in = Material.class.getResourceAsStream("material.properties")) {
			result.load(in);
		}
		return result;
	}

	public BufferedImage getTexture() {
		return texture;
	}
}
